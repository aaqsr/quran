#include <qapplication.h>
#include "QuranApp.h"

int main( int argc, char ** argv )
{
    QApplication a( argc, argv );
    QuranApp w;;
    w.show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}
