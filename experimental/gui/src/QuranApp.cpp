#include "QuranApp.h"

//#include <qapplication.h>
//#include <qsignal.h>

#include <qcdestyle.h>
#include <qplatinumstyle.h>
#include <qsgistyle.h>
#include <qcompactstyle.h>
#include <qinterlacestyle.h>
#include <qwindowsstyle.h>
#include <qmotifplusstyle.h>
#include <qwindowsxpstyle.h>
#include <qmotifstyle.h>
#include <qlayout.h>


#include <qlabel.h>
#include <qmenubar.h>
#include <qaction.h>
#include <qradiobutton.h>
#include <qlineedit.h>
#include <qspinbox.h>
#include <qcombobox.h>
#include <qtextbrowser.h>
//#include <quran.h>

#include "qapplication.h"
#include "QuranPaper.h"

QuranApp* QuranApp::instance = NULL;

QuranApp *QuranApp::getInstance() {
    return  instance;
}

QuranApp::QuranApp(QWidget *parent, const char *name, WFlags fl) : MainWindow(parent, name, fl),
showedFrame(NULL),
m_qurandata(NULL) {
    
    /* Initialize LibQuran */
    m_libquran.initialized = 0;
    int init_value=quran_init(&m_libquran);
    
    /* Initializing of LibQuran succeeded? */
    if (init_value!=LIBQURAN_SUCCEEDED || !m_libquran.initialized  ) {
	qFatal("Error calling quran_init(), Return Code:%d", init_value);
	exit(1);
    }
    
    /*  Language packages found? */
    if (!m_libquran.libquran.num_langs) {
	qFatal("No language packages are available");
	exit(1);
    }
    
    /* Setting a default style for QtQuran. Other styles may make QtQuran interface uglier */
    QApplication::setStyle( new QPlatinumStyle() );
    
    /* Prevent reversed direction. I don't know if this is a good idea. can be changed later (TODO) */
    qApp->setReverseLayout(FALSE);
    
    /* Hide all widgets related to buttons */
    frameGoTo->hide();
    frameSearch->hide();
    frameSearch2->hide();
    frameSearch3->hide();
    frameSearch4->hide();
    frameRecite->hide();
    frameExplain->hide();
    frameTranslate->hide();
    frameOptions->hide();
    searchSelection->hide();
    searchSection->hide();
    
    /* Set default size smaller and maximize the window */
    setGeometry( QRect(0, 0, 760, 440) );
    showMaximized();
    
    //  d_mode = Chapter;
    setRightJustification(TRUE);
    setCaption(trUtf8("Arabeyes Holy Qur'an Program - Version ") + VERSION);
    
    // Create a list of all translations
    for (int counter=0; counter<m_libquran.libquran.num_langs; counter++) {
        cbLanguage->insertItem( trUtf8( (QString)m_libquran.libquran.avail_langs[counter].name ) );
    }
    
    loadQuranData(0,0);
    
    //  qu = NULL;
    //  languageChanged(0);
    
//    quran * qu=quran_open_index(0,0, &m_libquran);
//    if (qu != NULL) {
//	cbChapter->clear();
//	for (int iix=0; iix<AMOUNT_SURAS; iix++)
//	    cbChapter->insertItem( QString::number(iix) + QString::fromLatin1(": ") + QString::fromUtf8((QString)qu->sura_titles[iix]) );
//	cbVerse->clear();
//	for (int t=1; t<8; t++)
//	    cbVerse->insertItem( QString::number(t) );
//	QString str;
//	for (int iix=0; iix<AMOUNT_SURAS; iix++)
//	    str += QString::number(iix) + QString::fromLatin1(": ") + QString::fromUtf8((QString)qu->sura_titles[iix]) + "\n";
//	tbQuranText->setText(str);
//    }
    /*    QString strtmp;
  cbsura->clear();
  
  for (int iix=0; iix<114; iix++)
    cbsura->insertItem( QString::fromUtf8((QString)qu->sura_titles[iix]) );
  cbaya->clear();
  for (int t=1; t<8; t++)
    cbaya->insertItem( QString::number(t) );
*/
    //  tlsajda->setText( trUtf8( "Sajda" ) );
    //  tlsajda->hide();
    
    //  tbQuranText->setTextFormat(Qt::RichText);
    //  tbQuranText->setWordWrap(QTextEdit::WidgetWidth);
    //  tbQuranText->setWrapPolicy(QTextEdit::AtWordBoundary);
    
    // signals and slots connections
    //  connect( tbQuranText, SIGNAL(clicked(int, int) ), this, SLOT( tbQuranTextClicked(int, int) ) );
    //  athread.qa = NULL;
    //  athread.q = this;    
/*    instance=this;
    m_quranPaper = new QuranPaper();
    QString str="";
    for (int i=0; i<100; i++) {
	str+= "<p><a name=\"" + QString::number(i) + "\">" + QString::number(i) + "</a></p>";
    }
    str+= "<a name=\"target\">target</a><br>";
    str+= "<a href=\"target.qml#40\">go</a>";
    m_quranPaper->setText(str, 100);
    */
}

QuranApp::~QuranApp() {
    quran_free();
    quran_close(m_qurandata, &m_libquran);
    delete m_quranPaper;
}

void QuranApp::loadQuranData(int lang, int file) {
    m_qurandata=quran_open_index(lang, file, &m_libquran);
    if (m_qurandata != NULL) {
	cbChapter->clear();
	for (int iix=0; iix<AMOUNT_SURAS; iix++)
	    cbChapter->insertItem( QString::number(iix) + QString::fromLatin1(": ") + QString::fromUtf8((QString)m_qurandata->sura_titles[iix]) );
	cbVerse->clear();
	for (int t=1; t<8; t++)
	    cbVerse->insertItem( QString::number(t) );
	QString str;
	for (int iix=0; iix<AMOUNT_SURAS; iix++)
	    str += "<p><a name=\"" + QString::number(i) + "\">" + QString::number(iix) + QString::fromLatin1(": ") + QString::fromUtf8((QString)qu->sura_titles[iix]) + "\n" + "</a></p>";
	m_quranPaper->setText(str, AMOUNT_SURAS);
    }
}

void QuranApp::onCancel() {
    frameGoTo->hide();
    frameSearch->hide();
    frameSearch2->hide();
    frameSearch3->hide();
    frameSearch4->hide();
    frameRecite->hide();
    frameExplain->hide();
    frameTranslate->hide();
    frameOptions->hide();
}

void QuranApp::onSearchNext() {    
    if (frameSearch->isShown()) {
	frameSearch->hide();
	frameSearch2->show();
	showedFrame=frameSearch2;
    } else if (frameSearch2->isShown()) {
	frameSearch2->hide();
	frameSearch3->show();
	showedFrame=frameSearch3;
    } else if (frameSearch3->isShown()) {
	frameSearch3->hide();
	frameSearch4->show();
	showedFrame=frameSearch4;
    }
    
}

void QuranApp::onAbout() {
    // TODO: Show about screen
    qWarning("%d - %d", geometry().width(), geometry().height());
}

void QuranApp::onSearch() {
    // Reset search widgets
    ibSearchText->setText("");
    rbCompletePhrase->setChecked(TRUE);
    rbExactMatch->setChecked(TRUE);
    ibSimilarity->setValue(80);
    ibSimilarity->setEnabled(FALSE);
    rbWhole->setChecked(TRUE);
    // Show/hide search widgets
    handleFrame(frameSearch);
}

void QuranApp::onExit() {
    qApp->exit();
}

void QuranApp::onNextPage() {
    m_quranPaper->goToNextPage();
}

void QuranApp::onNextChapter() {
    m_quranPaper->goToNextParagraph();
}
 

void QuranApp::onPrevPage() {
    m_quranPaper->goToPreviousPage();
}

void QuranApp::onPrevChapter() {
//    m_quranPaper->goToPreviousParagraph();
}

void QuranApp::onRecite() {
    handleFrame(frameRecite);
}

void QuranApp::onExplain() {
    handleFrame(frameExplain);
}

void QuranApp::onTranslate() {
    handleFrame(frameTranslate);
}

void QuranApp::onHelp() {}
void QuranApp::onLicense() {}

void QuranApp::onGoTo() {
    handleFrame(frameGoTo);
}

void QuranApp::onIndex() {}

void QuranApp::onOptions() {
    handleFrame(frameOptions);
}

void QuranApp::showEvent( QShowEvent *e )  {
    
    
}

void QuranApp::mouseReleaseEvent( QMouseEvent *e ) {
    
    QFrame *frame = NULL;

    // Find the frame related to the pushed botton
    if (btnIndex->hasMouse()) {
	onIndex();
    } else if (btnGoTo->hasMouse()) {
	onGoTo();
    } else if (btnSearch->hasMouse()) {
	onSearch();
    } else if (btnRecite->hasMouse()) {
	onRecite();
    } else if (btnExplain->hasMouse()) {
	onExplain();
    } else if (btnTranslate->hasMouse()) {
	onTranslate();
    } else if (btnOptions->hasMouse()) {
	onOptions();
    } else if (btnQuit->hasMouse()) {
	onExit();
    } else if (btnNextPage->hasMouse() ) {
	onNextPage();
    } else if (btnNextChapter->hasMouse() ) {
	onNextChapter();
    } else if (btnPrevPage->hasMouse()) {
	onPrevPage();
    } else if (btnPrevChapter->hasMouse()) {
	onPrevChapter();
    }
    
}

void QuranApp::handleFrame(QFrame *frame) {
    
    if (showedFrame!=frame && showedFrame!=NULL) {
	showedFrame->hide();
    }
    
    if (frame) {
	// Show and hide frames
	if (frame->isShown()) {
	    frame->hide();
	    showedFrame=NULL;
	} else {
	    frame->show();
	    showedFrame=frame;
	}
    }
    
}

void QuranApp::onDoSearch() {
    
}

void QuranApp::onDoExplain() {
    
}

void QuranApp::onDoTranslate() {
    
}

void QuranApp::onDoGoTo() {
    
}

void QuranApp::onDoRecite() {
    
}

void QuranApp::onOptionChanged() {
    
    if (frameSearch3->isShown()) {
	if (rbExactMatch->isChecked()) {
	    ibSimilarity->setEnabled(FALSE);
	} else if (rbSimilarMatch->isChecked()) {
	    ibSimilarity->setEnabled(FALSE);
	} else if (rbSimilarity->isChecked()) {
	    ibSimilarity->setEnabled(TRUE);
	}
    }
    
    if (frameSearch4->isShown()) {
	if (rbWhole->isChecked()) {
	    searchSelection->hide();
	    searchSection->hide();
	    searchEmpty->show();
	} else if (rbPart->isChecked()) {
	    searchEmpty->hide();
	    searchSection->hide();
	    searchSelection->show();
	} else if (rbHezb->isChecked()) {
	    searchEmpty->hide();
	    searchSection->hide();
	    searchSelection->show();
	} else if (rbChapter->isChecked()) {
	    searchEmpty->hide();
	    searchSection->hide();
	    searchSelection->show();
	} else if (rbSection->isChecked()) {
	    searchEmpty->hide();
	    searchSelection->hide();
	    searchSection->show();
	}
    }
}

void QuranApp::onSelection() {
    tbQuranText->removeSelection();
}
