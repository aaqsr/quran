using System;
using LibQuran.StaticData;
namespace LibQuran.Ayah {
   internal class AyahInst {
      private int ayahnum;
      public String ayahtext;
      public String searchtext;

      public AyahInst(int ayah){
         ayahnum = ayah;
         ayahtext = "";
         searchtext = "";
      }
   }
}
