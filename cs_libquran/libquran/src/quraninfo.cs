using System;
using System.IO;
using LibQuran.Lang;
using LibQuran.Instance;

namespace LibQuran {
   public class QuranInfo {
      private string errmsg;
      private string datapath;
      private QuranLang[] langs;

      public QuranInfo(){
         errmsg = "";
         datapath = "";
      }

      public string getDataPath(){ return datapath; }
      public string getError(){ return errmsg; }
      public void setDataPath(string path){ datapath = path; }

      public bool setDataFile(string quranrc){
         try {
            if (!File.Exists(quranrc)){
               errmsg = "error!  " + datapath + " does not exist!";
               return false;
            }
            FileStream file = new FileStream(quranrc, 
                  FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader sr = new StreamReader(file);
            datapath = sr.ReadToEnd();
            datapath = datapath.Trim();
            return true;
         }
         catch (Exception e){
            errmsg = "error!  unable to open .quranrc file: " + quranrc;
            return false;
         }
      }

      public bool init(){
         if ((datapath == null) || (datapath.Length == 0)){
            errmsg = "error!  you must set the datapath first!";
            return false;
         }

         DirectoryInfo di = new DirectoryInfo(datapath);
         DirectoryInfo[] dirs = di.GetDirectories();
         if (dirs.Length == 0){
            errmsg = "no files found in given datapath: " + datapath;
            return false;
         }

         int cnt = 0;
         for (int i=0; i<dirs.Length; i++){
            string fp = datapath + Path.DirectorySeparatorChar +
               dirs[i].Name + Path.DirectorySeparatorChar + "conf.xml";
            if (!File.Exists(fp))
               dirs[i] = null;
            else cnt++;
         }
         if (cnt == 0){
            errmsg = "couldn't find conf.xml file in any of the potential"
               + " quran data directories under datapath of: " + datapath;
            return false;
         }

         langs = new QuranLang[cnt];
         for (int i=0, j=0; i<dirs.Length; i++)
            if (dirs[i]!=null)
               langs[j++] = new QuranLang(datapath + 
                     Path.DirectorySeparatorChar + dirs[i].Name);
         for (int i=0; i<langs.Length; i++)
            langs[i].init();
         return true;
      }

      public string[] getLanguageCodes(){
         string[] lcodes = new string[langs.Length];
         for (int i=0; i<langs.Length; i++)
            lcodes[i] = langs[i].getCode();
         return lcodes;
      }

      public string[] getLanguageNames(){
         string[] lnames = new string[langs.Length];
         for (int i=0; i<langs.Length; i++)
            lnames[i] = langs[i].getName();
         return lnames;
      }

      public string[] getLanguageTexts(string language){
         for (int i=0; i<langs.Length; i++)
            if (language.CompareTo(langs[i].getName())==0)
               return langs[i].getTextList();
         for (int i=0; i<langs.Length; i++)
            if (language.CompareTo(langs[i].getCode())==0)
               return langs[i].getTextList();
         return null;
      }

      public string[] getLanguageAudio(string language){
         for (int i=0; i<langs.Length; i++)
            if (language.CompareTo(langs[i].getName())==0)
               return langs[i].getAudioList();
         for (int i=0; i<langs.Length; i++)
            if (language.CompareTo(langs[i].getCode())==0)
               return langs[i].getAudioList();
         return null;
      }

      public QuranInst openText(string language, string textname){
         for (int i=0; i<langs.Length; i++)
            if (language.CompareTo(langs[i].getName())==0)
               return langs[i].openText(textname);
         for (int i=0; i<langs.Length; i++)
            if (language.CompareTo(langs[i].getCode())==0)
               return langs[i].openText(textname);
         return null;
      }
   }
}
