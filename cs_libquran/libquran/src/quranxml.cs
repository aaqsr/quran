using System;
using System.Xml;
using LibQuran.Instance;

namespace LibQuran.XMLParse {
   public class QuranXML {
      private QuranInst q;
      private string filename;
      private int sura, ayah, current = 0;
      
      public QuranXML(QuranInst q, string filename){
         this.q = q;
         this.filename = filename;
      }

      public void parse(){
         int status = 0;
         XmlTextReader xtr = new XmlTextReader(filename);
         while (xtr.Read()){
            if (xtr.NodeType == XmlNodeType.Element){
               if (xtr.Name == "translated") status = 1;
               else if (xtr.Name == "sura") status = 2;
               else if (xtr.Name == "aya") status = 3;
               else if (xtr.Name == "qurantext") status = 4;
               else if (xtr.Name == "searchtext") status = 5;
               else status = 0;

               string name = "";
               while (xtr.MoveToNextAttribute()){
                  if ((status == 1) && (xtr.Name == "by"))
                     q.translator = xtr.Value;
                  else if ((status == 2) && (xtr.Name == "id"))
                     sura = int.Parse(xtr.Value);
                  else if ((status == 2) && (xtr.Name == "name"))
                     name = xtr.Value;
                  else if ((status == 3) && (xtr.Name == "id"))
                     ayah = int.Parse(xtr.Value);
               }
               xtr.MoveToElement();
               if (status == 2) q.addSura(sura, name);
            }
            else if (xtr.NodeType == XmlNodeType.Text){
               if (status == 4) q.addAyah(sura, ayah, xtr.Value);
               else if (status == 5) q.addSearchText(sura, ayah, xtr.Value);
            }
         }
      }
   }
}
