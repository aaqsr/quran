using System;

namespace LibQuran.QuranLangData {
   public class QuranAudio {
      private string name;
      private string author;
      private string audiodir; 
      private string copyright;

      public QuranAudio(){
         author = "";
         audiodir = "";
         copyright = "";
      }

      public string getAudioDir(){ return audiodir; }
      public string getName(){ return name; }
      public string getAuthor(){ return author; }
      public string getCopyright(){ return copyright; }

      public void setAuthor(string auth){ author = auth; }
      public void setCopyright(string cr){ copyright = cr; }
      public void setAudioDir(string fn, string path){
         name = fn;
         audiodir = path;
      }
   }
}
