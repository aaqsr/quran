using System;
using LibQuran.Ayah;
using LibQuran.StaticData;

namespace LibQuran.Sura {
   internal class SuraInst {
      public int suranum;
      public string suraname;
      private AyahInst[] ayahlist;

      public SuraInst(int number, String name){
         suranum = number;
         suraname = name;
         ayahlist = new AyahInst[QuranData.NUMAYAHS[number-1]];
         for (int i=1; i<=QuranData.NUMAYAHS[number-1]; i++)
            ayahlist[i-1] = new AyahInst(i);
      }

      public void addAyah(int ayahnum, string ayahtext){
         ayahlist[ayahnum-1].ayahtext = ayahtext;
      }

      public void addSearchText(int ayahnum, string searchtext){
         ayahlist[ayahnum-1].searchtext = searchtext;
      }

      public string getAyah(int ayahnum){
         return ayahlist[ayahnum-1].ayahtext;
      }

      public string getSearchText(int ayahnum){
         return ayahlist[ayahnum-1].searchtext;
      }
   }
}
