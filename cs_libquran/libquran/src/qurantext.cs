using System;
using LibQuran.Instance;

namespace LibQuran.QuranLangData {
   public class QuranText {
      private string name;
      private string author;
      private string filepath;
      private string copyright;

      public QuranText(){
         author = "";
         filepath = "";
         copyright = "";
      }

      public QuranInst open(){
         QuranInst qi = new QuranInst();
         if (!qi.init(filepath)) return null;
         else return qi;
      }

      public string getFilePath(){ return filepath; }
      public string getName(){ return name; }
      public string getAuthor(){ return author; }
      public string getCopyright(){ return copyright; }

      public void setAuthor(string auth){ author = auth; }
      public void setCopyright(string cr){ copyright = cr; }
      public void setFilePath(string fn, string path){
         name = fn;
         filepath = path;
      }
   }
}
