using System;
using System.IO;
using System.Xml;
using System.Collections;
using LibQuran.Instance;
using LibQuran.QuranLangData;

namespace LibQuran.Lang {
   public class QuranLang {
      private ArrayList audio;
      private ArrayList texts;
      private string langcode;
      private string langname;
      private string langpath;

      private int audio_ctr;
      private int texts_ctr;

      public QuranLang(string path){
         langpath = path;
         langcode = "";
         langname = "";
         audio = new ArrayList();
         texts = new ArrayList();
      }

      public void init(){
         try {
            audio_ctr = -1;
            texts_ctr = -1;
            string fn = langpath + Path.DirectorySeparatorChar + "conf.xml";
            XmlTextReader xtr = new XmlTextReader(fn);

            int status = 0;
            int curtext = 0;
            while (xtr.Read()){
               if (xtr.NodeType == XmlNodeType.Element){
                  if (xtr.Name == "language"){ status = 1; }
                  if (xtr.Name == "text"){ this.newText(); status = 2; }
                  if (xtr.Name == "audio"){ this.newAudio(); status = 3; }
                  if (xtr.Name == "author"){ curtext = 1; }
                  if (xtr.Name == "copyright"){ curtext = 2; }
                  if (xtr.Name == "filename"){ curtext = 3; }

                  while (xtr.MoveToNextAttribute()){
                     if ((status == 1) && (xtr.Name == "code"))
                        this.setCode(xtr.Value);
                     else if ((status == 1) && (xtr.Name == "name"))
                        this.setName(xtr.Value);
                  }
                  xtr.MoveToElement();
               }
               else if (xtr.NodeType == XmlNodeType.Text){
                  if ((curtext == 1) && (status == 2))
                     this.addTextAuthor(xtr.Value);
                  else if ((curtext == 1) && (status == 3))
                     this.addAudioAuthor(xtr.Value);
                  else if ((curtext == 2) && (status == 2))
                     this.addTextCopyright(xtr.Value);
                  else if ((curtext == 2) && (status == 3))
                     this.addAudioCopyright(xtr.Value);
                  else if ((curtext == 3) && (status == 2))
                     this.addTextFilepath(xtr.Value);
                  else if ((curtext == 3) && (status == 3))
                     this.addAudioDir(xtr.Value);

                  curtext = 0;
               }
            }
            audio.TrimToSize();
            texts.TrimToSize();
            return;
         }
         catch (Exception e){
            audio = new ArrayList();
            texts = new ArrayList();
            audio.TrimToSize();
            texts.TrimToSize();
            Console.WriteLine("error encountered while parsing conf.xml!");
            Console.WriteLine(e.StackTrace);
            return;
         }
      }

      public void newText(){
         texts_ctr++;
         texts.Add(new QuranText());
         texts.TrimToSize();
      }

      public void newAudio(){
         audio_ctr++;
         audio.Add(new QuranAudio());
         audio.TrimToSize();
      }

      public void addTextAuthor(string auth){
         QuranText qt = null;
         IEnumerator e = texts.GetEnumerator();
         while (e.MoveNext())
            qt = ((QuranText)(e.Current));
         qt.setAuthor(auth);
      }

      public void addAudioAuthor(string auth){
         QuranAudio qa = null;
         IEnumerator e = audio.GetEnumerator();
         while (e.MoveNext())
            qa = ((QuranAudio)(e.Current));
         qa.setAuthor(auth);
      }

      public void addTextCopyright(string cpinfo){
         QuranText qt = null;
         IEnumerator e = texts.GetEnumerator();
         while (e.MoveNext())
            qt = ((QuranText)(e.Current));
         qt.setCopyright(cpinfo);
      }

      public void addAudioCopyright(string cpinfo){
         QuranAudio qa = null;
         IEnumerator e = audio.GetEnumerator();
         while (e.MoveNext())
            qa = ((QuranAudio)(e.Current));
         qa.setCopyright(cpinfo);
      }

      public void addTextFilepath(string fp){
         QuranText qt = null;
         IEnumerator e = texts.GetEnumerator();
         while (e.MoveNext())
            qt = ((QuranText)(e.Current));
         qt.setFilePath(fp, langpath + Path.DirectorySeparatorChar + fp);
      }

      public void addAudioDir(string fp){
         QuranAudio qa = null;
         IEnumerator e = audio.GetEnumerator();
         while (e.MoveNext())
            qa = ((QuranAudio)(e.Current));
         qa.setAudioDir(fp, langpath + Path.DirectorySeparatorChar + fp);
      }

      public string[] getTextList(){
         if (texts.Capacity == 0) return null;
         int i = 0;
         string[] toret = new string[texts.Capacity];
         IEnumerator e = texts.GetEnumerator();
         while (e.MoveNext())
            toret[i++] = ((QuranText)(e.Current)).getName();
         return toret;
      }

      public string[] getAudioList(){
         if (audio.Capacity == 0) return null;
         int i = 0;
         string[] toret = new string[audio.Capacity];
         IEnumerator e = texts.GetEnumerator();
         while (e.MoveNext())
            toret[i++] = ((QuranAudio)(e.Current)).getName();
         return toret;
      }

      public QuranInst openText(string textname){
         if (texts.Capacity == 0) return null;
         IEnumerator e = texts.GetEnumerator();
         while (e.MoveNext())
            if (textname.CompareTo(((QuranText)(e.Current)).getName())==0)
               return ((QuranText)e.Current).open();
         return null;
      }

      public void setCode(string code){ langcode = code; }
      public void setName(string name){ langname = name; }
      public string getCode(){ return langcode; }
      public string getName(){ return langname; }
   }
}
