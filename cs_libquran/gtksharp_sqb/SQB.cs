using System;
using System.IO;

using GLib;
using Gdk;
using Gtk;
using GtkSharp;
using Glade;

using LibQuran;
using LibQuran.Instance;


class sqb {
   private int sura, ayah;
   private QuranInst aq;
   private Gtk.TextBuffer buffer;
   
   [Glade.Widget]
      Gtk.TextView textview1;

   [Glade.Widget]
      Gtk.Label label1;
   
   public sqb(string path){
      Glade.XML gui = new Glade.XML(null, "sqb.glade", "window1", null);
      gui.Autoconnect(this);

      QuranInfo qi = new QuranInfo();
      if (!qi.setDataFile(path + Path.DirectorySeparatorChar + ".quranrc")){
         Console.WriteLine("error! " + qi.getError());
         return;
      }
      qi.init();
      aq = qi.openText("ar", "quran.ar.xml");
      if (aq == null)
         Console.WriteLine("error loading quran xml file: {0}", aq.errmsg);

      ayah = 1;
      sura = 1;
      buffer = textview1.Buffer;
      displayAyah();
   }

   public void displayAyah(){
      if (aq == null) return;
      label1.Text = "sura: " + sura + " (" + aq.getSuraName(sura) + 
         ") ayah: " + ayah + "  juz: " + aq.getLocationJuz(sura, ayah) +
         (aq.isSajdah(sura, ayah)? " sajda" : "");
      buffer.Text = aq.getAyah(sura, ayah);
   }
   
   public void jumpTo(int tmpsura, int tmpayah){
      if (aq == null) return;
      if ((tmpsura < 0) || (tmpsura > 114))
         return;
      else if (tmpayah > aq.getNumAyahs(tmpsura))
         return;
      else {
         sura = tmpsura;
         ayah = tmpayah;
         displayAyah();
      }
   }
   
   public void on_next_button_clicked(object o, EventArgs e){
      if (aq == null) return;
      
      ayah++;
      if (ayah > aq.getNumAyahs(sura)){
         if (sura == 114) sura = 1;
         else sura++;
         ayah = 1;
      }
      displayAyah();
   }
   
   public void on_previous_button_clicked(object o, EventArgs e){
      if (aq == null) return;

      ayah--;
      if (ayah==0){
         if (sura == 1) sura = 114;
         else sura--;
         ayah = aq.getNumAyahs(sura);
      }
      displayAyah();
   }
   
   public void on_quit1_activate(object o, EventArgs e){
      Application.Quit();
   }

   public void on_jump1_activate(object o, EventArgs e){
      new jumpdialog(this);
   }

   public static void Main(string[] argv){
      if (argv.Length != 1){
         Console.WriteLine("run with a parameter of the directory which "
               + "contains the .quranrc file.");
         return;
      }
      Application.Init();
      new sqb(argv[0]);
      Application.Run();
   }
}

class jumpdialog {
   private sqb s;

   [Glade.Widget]
      Gtk.Window window1;
   [Glade.Widget]
      Gtk.Entry sura_entry;
   [Glade.Widget]
      Gtk.Entry ayah_entry;

   public jumpdialog(sqb sqbinst){
      s = sqbinst;
      Glade.XML gui = new Glade.XML(null, "jumpto.glade", "window1", null);
      gui.Autoconnect(this);
   }

   public void on_jump_button_clicked(object o, EventArgs e){
      try {
         int tmpsura, tmpayah;
         tmpsura = int.Parse(sura_entry.Text);
         tmpayah = int.Parse(ayah_entry.Text);
         s.jumpTo(tmpsura, tmpayah);
         window1.Destroy();
      }
      catch (Exception ex){
         window1.Destroy();
      }
   }

   public void on_cancel_button_clicked(object o, EventArgs e){
      window1.Destroy();
   }
}
