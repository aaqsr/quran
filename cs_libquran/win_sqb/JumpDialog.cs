using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace sqb
{
	/// <summary>
	/// Summary description for JumpDialog.
	/// </summary>
	public class JumpDialog : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox suraInput;
		private System.Windows.Forms.TextBox ayahInput;
		private System.Windows.Forms.Button jumpButton;
		private SQBForm sqf;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public JumpDialog(SQBForm sqf)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.sqf = sqf;
			this.Show();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.suraInput = new System.Windows.Forms.TextBox();
			this.ayahInput = new System.Windows.Forms.TextBox();
			this.jumpButton = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.jumpButton,
																					this.ayahInput,
																					this.suraInput,
																					this.label2,
																					this.label1});
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(264, 88);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Enter Sura and Ayah to Jump To";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Sura";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.TabIndex = 1;
			this.label2.Text = "Ayah";
			// 
			// suraInput
			// 
			this.suraInput.Location = new System.Drawing.Point(112, 16);
			this.suraInput.MaxLength = 3;
			this.suraInput.Name = "suraInput";
			this.suraInput.Size = new System.Drawing.Size(144, 20);
			this.suraInput.TabIndex = 2;
			this.suraInput.Text = "";
			// 
			// ayahInput
			// 
			this.ayahInput.Location = new System.Drawing.Point(112, 40);
			this.ayahInput.MaxLength = 3;
			this.ayahInput.Name = "ayahInput";
			this.ayahInput.Size = new System.Drawing.Size(144, 20);
			this.ayahInput.TabIndex = 3;
			this.ayahInput.Text = "";
			// 
			// jumpButton
			// 
			this.jumpButton.Location = new System.Drawing.Point(8, 64);
			this.jumpButton.Name = "jumpButton";
			this.jumpButton.Size = new System.Drawing.Size(256, 23);
			this.jumpButton.TabIndex = 4;
			this.jumpButton.Text = "Jump!";
			this.jumpButton.Click += new System.EventHandler(this.jumpButton_Click);
			// 
			// JumpDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(264, 93);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.groupBox1});
			this.Name = "JumpDialog";
			this.Text = "JumpDialog";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void jumpButton_Click(object sender, System.EventArgs e)
		{
			sqf.jumpTo(int.Parse(suraInput.Text), int.Parse(ayahInput.Text));
			this.Dispose();		
		}
	}
}
