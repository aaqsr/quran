/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public Licquran_sura_infoense for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#ifndef __QURANDATA_H__
#define __QURANDATA_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef XmlParse_INCLUDED
#include <expat.h>
#endif

#ifndef __QURAN_H__
#include "quran.h"
#endif

/**
 * data structure used temporarily when parsing
 * XML file.
 **/
typedef struct {
	int	sura_no, aya_no;
	quran *q;
	aya *last_aya, *last_search;
        char * text;
} q_temp;

/**
 * data structure stores a pair of (Sura/Aya) used in determining
 * the place of some verses.
 **/
typedef struct {
	int sura_no; /*!< Sura number */
	int aya_no; /*!< Aya number */
} SuraAya;

/*! Contains positions of sajdas. */
extern const SuraAya sajda[14];
/*! contains positions of hezb quads. */
extern const SuraAya hezb_quad[240];
/*! contains the amount ayas in every suras. */
extern const int aya_numbers[AMOUNT_SURAS];


/* prototypes for Internal functions */
int build_sura(quran *);
int callback_enc(void *enc_data, const XML_Char *name, XML_Encoding *info);
void callback_start(void *, const XML_Char *, const XML_Char **);
void callback_cdata(void *, const XML_Char *, int);
void callback_end(void *, const XML_Char *);
void free_aya(aya *);

#ifdef __cplusplus
}
#endif

#endif  // !__QURANDATA_H__
