/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#include <string.h>

#include "quran.h"
#include "globals.h"

#include "init.h"


/**
 * Internal function: Converts a string to low case.
 *
 * NOTE: This function doesn't return a new string. It execute changes directly
 * on the given string (str)
 *
 * \param str string to convert
 *
 * \return the converted string
 **/
char *lcase(char *str) {

	int i;

	/* Change evey character to it low case */
	for (i = 0; i < strlen(str); i++) {
		str[i] = tolower(str[i]);
	}

	return str;
}

/**
 * Internal function: Converts a two-letters language code to an index of
 * libquran.avail_langs.
 *
 * \param code the two-letters language code to be converted
 * \param libquran_ctx quran context
 *
 * \return the converted index
 **/
int langcode_to_index(const char * code, libquran_ctx *ctx) {
	int i;

	/* Check each package to get the index of the right one */
	for (i = 0; i < ctx->libquran.num_langs; i++)
		if (strncmp(ctx->libquran.avail_langs[i].code, code, 2) == 0)
		    return i;
	return LIBQURAN_NO_SUCH_LANG;
}

/**
 * Internal function: Converts an audio package name to an index of
 * libquran.avail_langs[].avail_audio_packages.
 *
 * \param lang the index of the language package containing that audio package.
 * \param package the audio package name to be converted.
 * \param libquran_ctx quran context
 *
 * \return the converted index
 **/
int audiopackage_to_index(int lang, const char * package, libquran_ctx *ctx) {
	int i;

	if (ctx->libquran.num_langs < (lang+1))
	    return LIBQURAN_NO_SUCH_LANG;

	/* Check each package to get the index of the right one */
	for (i = 0; i < ctx->libquran.avail_langs[lang].num_audio_packages; i++)
		if (strcmp(ctx->libquran.avail_langs[lang].avail_audio_packages[i].filename, package) == 0)
		    return i;
	return LIBQURAN_NO_SUCH_AUDIO_PACKAGE;
}

/**
 * Internal function: Converts a text file name to an index of
 * libquran.avail_langs[].avail_texts.
 *
 * \param lang the index of the language package containing that audio package.
 * \param text_file the text file name to be converted.
 * \param libquran_ctx quran context
 *
 * \return the converted index
 **/
int text_file_to_index(int lang, const char * text_file, libquran_ctx *ctx) {
	int i;

	if (ctx->libquran.num_langs < (lang+1))
	    return LIBQURAN_NO_SUCH_LANG;
	/* Check each text to get the index of the right one */
	for (i = 0; i < ctx->libquran.avail_langs[lang].num_texts; i++) {
	    if (strcmp(ctx->libquran.avail_langs[lang].avail_texts[i].filename, text_file) == 0)
		return i;
	}
	return LIBQURAN_NO_SUCH_TEXT;
}
