/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public Licquran_sura_infoense for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#ifndef __QURANAUDIO_H__
#define __QURANAUDIO_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef STEREO_H
#include <speex_stereo.h>
#endif

#ifndef _OGG_H
#include <ogg/ogg.h>
#endif

#ifndef __QURAN_H__
#include "quran.h"
#endif

/* prototypes for internal functions */
int quran_audio_play_further(quran_audio *, int (*)(quran_audio *));
int decodePacket(quran_audio *, ogg_packet *, SpeexStereoState *, int, int *);
static void *process_header(ogg_packet *, int, int *, int *, int *, int, int *, SpeexStereoState *);

#ifdef __cplusplus
}
#endif

#endif  // !__QURANAUDIO_H__
