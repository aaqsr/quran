/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public Licquran_sura_infoense for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#ifndef __QURAN_INIT_H__
#define __QURAN_INIT_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef XmlParse_INCLUDED
#include <expat.h>
#endif

#ifndef  _DIRENT_H
#include <dirent.h>
#endif

#ifndef __QURAN_H__
#include "quran.h"
#endif

#ifndef LIBQURAN_DEFAULT_DATADIR
#define LIBQURAN_DEFAULT_DATADIR "/usr/local/share/libquran" /*!< default path for data xml files */
#endif

/* prototypes for Internal functions */
int load_conf_file();
void callback_start_config(void *, const XML_Char *, const XML_Char **);
void callback_cdata_config(void *, const XML_Char *, int);
void callback_end_config(void *, const XML_Char *);
int generate_languages_list();
int select_lang_package(const char *, libquran_ctx *);

#ifdef __cplusplus
}
#endif

#endif  // !__QURAN_INIT_H__
