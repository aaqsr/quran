/* libquran - Holy Quran library 
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public Licquran_sura_infoense for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#ifndef __QURAN_H__
#define __QURAN_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#ifndef SPEEX_CALLBACKS_H
#include <speex/speex_callbacks.h>
#endif

#ifndef _OGG_H
#include <ogg/ogg.h>
#endif

/*! frame size */
#define MAX_FRAME_SIZE 2000

/*! amount of suras in the Holy Quran */
#define AMOUNT_SURAS 114

/*! amount of hezbs in the Holy Quran */
#define AMOUNT_HEZBS 60

/*! amount of Juzzes in the Holy Quran */
#define AMOUNT_JUZZ 30

/*! size of the buffer used by the XML parser */
#define BUFF_SIZE 10000

/* return values that can be returned by quran_init() */
#define LIBQURAN_SUCCEEDED              0   /*!< Initialization succeeded.  */
#define LIBQURAN_XML_FAILED            -1   /*!< Failed initializing the XML Parser. */
#define LIBQURAN_NO_DATADIR            -2   /*!< Failed opening the data directory. */
#define LIBQURAN_NO_LANGS              -3   /*!< Could not find any quran translation. */
#define LIBQURAN_NO_CONF               -4   /*!< Could not find the configuration file. */
#define LIBQURAN_NOT_INITIALIZED       -5   /*!< Failed initializing. */
#define LIBQURAN_ALREADY_INITIALIZED   -6   /*!< 'libquran' is already initialized. */
#define LIBQURAN_INVALID_PACKAGE_CONF  -8   /*!< Failed opening the conf.xml file for a language package. */
#define LIBQURAN_NO_SUCH_LANG	       -9   /*!< The language requested is not available. */
#define LIBQURAN_NO_SUCH_AUDIO_PACKAGE -10  /*!< The audio package requested is not available. */
#define LIBQURAN_NO_SUCH_TEXT	       -11  /*!< The text/translation requested is not available. */
#define LIBQURAN_NO_MEM				   -12 /*!< not enough memory */
#define LIBQURAN_FILE_OPEN_FAILED      -13 /*!< error opening file */
/* return values that can be returned by quran_search_* */
#define LIBQURAN_ERROR_INVALID_ARGUMENTS -14
#define LIBQURAN_ERROR_OUT_OF_RANGE -15

/*! Match exact (100 %). Used for similarity */
#define EXACT_MATCH 100

/**
 * Types of search.
 * \sa quran_search_quran(), quran_search_sura(), quran_search_hezb(),
 * quran_search_juzz(), quran_search_part()
 **/
enum searchTypes {
	ALL_WORDS, /*!< Match all of the words */
	ANY_WORD, /*!< Match any word */
	COMPLETE_PHRASE, /*!< Match complete phrase */
	PHONETICAL /*<! Phonetical search */
};

/**
 * data_component data structure used to store data about the various
 * language packages components (text, interpretation and audio).
 * \sa language
 **/
typedef struct {
	char *author;
	char *copyright;
	char *filename;
} data_component;

typedef struct {
	char *code; /*!< language code */
	char *name; /*!< language name */
        data_component *avail_audio_packages; /*!< vector of available audio packages */
	int  num_audio_packages; /*!< number of available audio packages */
	data_component *avail_texts; /*!< vector of available texts (The original text or translations) */
	int  num_texts; /*!< number of available texts */
	data_component *avail_tafsir; /*!< vector of available tafsir texts (interpretaions) */
	int  num_tafsir; /*!< number of available tafsir texts (interpretaions) */
} language;


/**
 * libquran data structure used to remember libquran properties.
 * \sa libquran_ctx
 **/
typedef struct {
	char *data_directory; /*!< directory name where data files reside*/
	language *avail_langs; /*!< dynamic array of available language packages */
	int  num_langs; /*!< number of available language packages */
} _libquran;

/**
 * aya data structure is a single linked list.
 * \sa quran
 **/
typedef struct _aya {
	char *text; /*!< the text of an aya */
	struct _aya *next; /*!< a pointer to the next aya */
} aya;

/**
 * quran data structure.
 * \sa quran_open(), quran_close()
 **/
typedef struct {
	char *lang; /*!< quran language */
	int  mediatype; /*!< quran media type */
	char *encoding; /*!< charset encoding */
	FILE *file_handle; /*!< quran XML file handle */
	aya  *sura[AMOUNT_SURAS]; /*!< pointer to first aya */
	aya  *sura_search[AMOUNT_SURAS]; /*!< pointer to first aya in search mode*/
	char *sura_titles[AMOUNT_SURAS]; /*!< array of sura titles */
	int  has_search_tag; /*!< 1 if has <searchtext> tag */
	char *credits; /*!< credits to XML preparer/translator */
} quran;

/**
 * ayainfo data structure used by quran_verse_info() to return info.
 * \sa quran_verse_info()
 **/
typedef struct {
  int juz_number; /*!< juzz number (1..30) */
  int hezb_number; /*!< hezb number (1..60) */
  int hezb_quad; /*!<  hezb quad number (1..4) */
  int has_sajda; /*!< is 1 if the aya has a sajda, otherwise 0 */
} ayainfo;

/**
 * sura_info data structure used by quran_sura_info() to return info about a sura.
 * \sa quran_sura_info()
 **/
typedef struct {
	int max_aya;
} sura_info;

/**
 * quran audio data structure.
 * \sa quran_audio_open(), quran_audio_play(), quran_audio_stop(), quran_audio_close()
 **/
typedef struct {
	int channels; /*!< number of channels */
	int rate; /*!< sample rate */
	FILE * spxfile; /*!< Speex handle */
	void * st;
	int frame_size;
	short buffer[MAX_FRAME_SIZE]; /*!< PCM audio buffer */
	ogg_sync_state oy;
	SpeexBits bits;
	int bitrate;
	int playing;
} quran_audio;

/**
 * A dynamic array used for storing the search results.
 * \sa quran_search_quran(), quran_search_sura(), quran_search_hezb(),
 * quran_search_juzz(), quran_search_part(), quran_get_search_results(),
 * quran_free_search_results()
 **/
typedef struct _results_struct{
	int sura_no;
	int aya_no;
	int simularity;
	struct _results_struct *next;
} results_struct;

typedef struct {
	_libquran libquran; /*!< quran handle */
	int config_step; /*!< current step during parsing the XML file */
	int initialized;  /*!< is context initialized */
	int parent_tag; /*<! used by callback to determine the last parent tag */
	char * text;
	results_struct **results; /*! A dynamic array used for storing the results */
	int error_code;
	int lang;
} libquran_ctx;

/* prototypes for published functions */
int quran_init(libquran_ctx *);
void quran_free();
quran *quran_open(const char *, const char *, libquran_ctx *);
quran *quran_open_index(int, int, libquran_ctx *);
char *quran_read_verse(const quran *, int, int, libquran_ctx *); 
void quran_close(quran *, libquran_ctx *);
ayainfo * quran_verse_info (int , int );
sura_info * quran_sura_info(int);
char * quran_recitor_name(const char *, const char *);
quran_audio * quran_audio_open(const char *, const char *, const int, const int, libquran_ctx *);
quran_audio * quran_audio_open_index(int, int, const int, const int, libquran_ctx *);
int quran_audio_play(quran_audio *, int (*)(quran_audio *));
void quran_audio_stop(quran_audio *);
void quran_audio_close(quran_audio *, libquran_ctx *);
int quran_search_quran(quran *, const char *, int, int, libquran_ctx *);
int quran_search_sura(quran *, const char *, int, int, int, libquran_ctx *);
int quran_search_hezb(quran *, const char *, int, int, int, libquran_ctx *);
int quran_search_juzz(quran *, const char *, int, int, int, libquran_ctx *);
int quran_search_part(quran *, const char *, int, int, int, int, int, int, libquran_ctx *);
results_struct **quran_get_search_results(libquran_ctx *);
void quran_free_search_results(libquran_ctx *);


#ifdef __cplusplus
}
#endif

#endif  // !__QURAN_H__
