/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#include <speex/speex.h>
#include <speex/speex_header.h>
#include <speex/speex_stereo.h>
#include <speex/speex_callbacks.h>
#include <ogg/ogg.h>

#include "quran.h"
#include "globals.h"
#include "qurandata.h"

#include "quranaudio.h"

#define le_short(s) ((short) ((unsigned short) (s) << 8) | ((unsigned short) (s) >> 8))

/********************************* Published functions ********************************/


/**
 * Prepares data structure for audible quran.
 * The audio who's information is prepared can be played with quran_audio_play()
 * and freed by quran_audio_close()
 *
 * \param lang language to open
 * \param package package to open
 * \param sura_no sura index
 * \param aya_no aya index
 * \param libquran_ctx quran context
 *
 * \return NULL when failed opening the audio file, otherwise quran_audio handle
 *
 * \sa quran_audio_open_index(), quran_audio_play(), quran_audio_stop(), quran_audio_close()
 **/
quran_audio *quran_audio_open(const char *lang, const char *package, const int sura_no, const int aya_no, libquran_ctx *ctx) {

	int lang_index;
	int audio_package_index;

	/* Check the required variables */
	if ( (ctx->initialized!=1)) {
		ctx->error_code = LIBQURAN_NOT_INITIALIZED;
	 	return NULL;
	}

	if (!lang || !package) {
		ctx->error_code = LIBQURAN_ERROR_INVALID_ARGUMENTS;
		return NULL;
	}

	lang_index = langcode_to_index(lang, ctx);
	if (lang_index == LIBQURAN_NO_SUCH_LANG) {
		ctx->error_code = LIBQURAN_NO_SUCH_LANG;
	    return NULL;
	}
	audio_package_index = audiopackage_to_index(lang_index, package, ctx);
	if (lang_index == LIBQURAN_NO_SUCH_LANG  ||  lang_index == LIBQURAN_NO_SUCH_AUDIO_PACKAGE) {
		ctx->error_code = lang_index;
	    return NULL;
	}

	return quran_audio_open_index(lang_index, audio_package_index, sura_no, aya_no, ctx);
}

/**
 * Prepares data structure for audible quran.
 * The audio who's information is prepared can be played with quran_audio_play()
 * and freed by quran_audio_close()
 *
 * \param lang language package index to open
 * \param audio audio package index to open
 * \param sura_no sura index
 * \param aya_no aya index
 * \param libquran_ctx quran context
 *
 * \return NULL when failed opening the audio file, otherwise quran_audio handle
 *
 * \sa quran_audio_open(), quran_audio_play(), quran_audio_stop(), quran_audio_close()
 **/
quran_audio *quran_audio_open_index(const int lang, const int audio, const int sura_no, const int aya_no, libquran_ctx *ctx) {

	quran_audio *qa=NULL;
	char *audio_filename;

	/* Check the required variables */
	if (ctx->initialized!=1) {
		ctx->error_code = LIBQURAN_NOT_INITIALIZED;
		return NULL;
	}
	
	if ((lang<0) || (audio<0)) {
		ctx->error_code = LIBQURAN_ERROR_INVALID_ARGUMENTS;
		return NULL;
	}

	if ( (ctx->libquran.num_langs<1) ||
			 (ctx->libquran.avail_langs[lang].num_audio_packages<1) ||
			 ((sura_no<1||sura_no>AMOUNT_SURAS) && sura_no && aya_no) ||
			 ((aya_no<1||(aya_no>aya_numbers[sura_no-1])) && sura_no && aya_no) ) {

		ctx->error_code = LIBQURAN_ERROR_INVALID_ARGUMENTS;
		return NULL;
	}

	/* Allocate the memory needed for the absolute path of the filename */
	audio_filename = (char*) malloc(12 /* strlen(s001a001.spx) */ +
						strlen(ctx->libquran.data_directory) +
						strlen(ctx->libquran.avail_langs[lang].code) +
						3 /* 3 slashes */ +
						strlen(ctx->libquran.avail_langs[lang].avail_audio_packages[audio].filename));

	/* Generate this absolute path of the filename */
#if defined(_WIN32) && defined(_MINGW)
	sprintf(audio_filename, "%s\\%s\\%s\\s%03da%03d.spx", ctx->libquran.data_directory,
#else
	sprintf(audio_filename, "%s/%s/%s/s%03da%03d.spx", ctx->libquran.data_directory,
#endif
		ctx->libquran.avail_langs[lang].code,
		ctx->libquran.avail_langs[lang].avail_audio_packages[audio].filename,
		sura_no,aya_no);
	/* Allocate memory for a new a quran_audio struct object */
	qa = (quran_audio *)malloc(sizeof(quran_audio));

	/* Open the Speex file and save its handle in quran_audio struct */
	qa->spxfile = fopen(audio_filename,"r");

	/* The absolute path of the filename is not needed anymore, so free it */
	free(audio_filename);

	if (qa->spxfile) {

		/*Init Ogg data struct*/
		ogg_sync_init(&qa->oy);
		speex_bits_init(&qa->bits);
		qa->st = NULL;
		qa->channels = -1;
		qa->rate = 0;
		qa->frame_size = 0;
		qa->buffer[MAX_FRAME_SIZE]=0;
		qa->playing = 0;

		return qa;

	} else {

		/* Could not open the Speex file */
		return NULL;

	}
}

/**
 * Loads and decodes the Speex audio data from the opened quran_audio and passes
 * it to the by user defined callback function. This callback function will be
 * called after every packet is decoded.
 * The user has to develop a callback function which can play the PCM data stored
 * in quran_audio
 *
 * \param qa quran_audio handle
 * \param callback user defined functions to handle decoded PCM data
 *
 * \return 0 if completed normally, otherwise a nonzero value
 *
 * \sa quran_audio_open(), quran_audio_stop(), quran_audio_close()
 **/
int quran_audio_play(quran_audio *qa, int (*callback)(quran_audio *)) {

	/* The callback function must exist */
	if (callback==NULL) {
		return;
	}
	
	int ret = 0;
	
	if (qa) {
		/* Play only after quran_audio_open() is called */
		if (qa->playing <= 0) {
			qa->playing = 1;
			ret = quran_audio_play_further(qa, callback);
		}
		quran_audio_stop(qa);
	}

	return ret;
}

/**
 * Stops the current playing process of PCM data.
 * NOTE: This function will not work when the callback method hangs.
 *
 * \param qa quran_audio handle
 *
 * \sa quran_audio_open(), quran_audio_play(), quran_audio_close()
 **/
void quran_audio_stop(quran_audio *qa) {

	if (qa->playing == 1) {
		qa->playing = 0;
	}
}

/**
 * Closes the by quran_audio_open() opened quran_audio.
 * NOTE: must be called on every by quran_audio_open() opened quran_audio
 *
 * \param qa quran_audio handle
 *
 * \param libquran_ctx quran context
 * \sa quran_audio_open()
 **/
void quran_audio_close(quran_audio *qa, libquran_ctx *ctx) {

	// TODO: Is it not better to check first if the audio is closed?
	// You know, accessing a deleted object can cause some beautiful crashes ;-)

	if (ctx->initialized!=1) {
		return;
	}

	if (qa) {
		free(qa);
		qa=NULL;
	}
}


/********************************** Internal functions **********************************/


/**
 * Internal function: Does the job of quran_audio_play() further. It loads and
 * decodes the Speex audio data from the opened quran_audio and passes it to
 * the by user defined callback function. This callback function will be called
 * after every packet is decoded.
 *
 * \param qa quran_audio handle
 * \param callback: user defined functions to handle decoded PCM data
 *
 * \return 0 if completed normally, otherwise a nonzero value
 *
 * \sa quran_audio_play()
 **/
int quran_audio_play_further(quran_audio *qa, int (*callback)(quran_audio *)) {
	
	int c; // TODO: This variable is not used!!!
	int option_index = 0; // TODO: This variable is not used!!!
	int packet_count = 0;
	ogg_page og;
	ogg_packet op;
	ogg_stream_state os;
	int enh_enabled=0;
	int nframes=2;
	int stream_init = 0;
	int close_in=0; // TODO: This variable is not used!!!
	int eos=0;
	int forceMode=-1;
	int audio_size=0; // TODO: The value of this variable is not used!!!
	SpeexStereoState stereo = SPEEX_STEREO_STATE_INIT;

	if (callback==NULL) {
		fprintf(stderr,"quran_audio_play: NULL callback\n");
		return -1;
	}

	if (qa) {

		while (qa->playing) {
			
			char *data;
			int nb_read;
			
			/*Get the ogg buffer for writing*/
			data = ogg_sync_buffer(&qa->oy, 200);
			
			/*Read bitstream from input file */
			nb_read = fread(data, sizeof(char), 200, qa->spxfile);

			/* Write ogg buffer */
			ogg_sync_wrote(&qa->oy, nb_read);
			
			/*Loop for all complete pages we got (most likely only one)*/
			while (ogg_sync_pageout(&qa->oy, &og)==1) {
				
				if (stream_init == 0) {
					ogg_stream_init(&os, ogg_page_serialno(&og));
					stream_init = 1;
				}
				
				/*Add page to the bitstream*/
				ogg_stream_pagein(&os, &og);
				/*Extract all available packets*/
				while (!eos && ogg_stream_packetout(&os, &op)==1) {
					/*If first packet, process as Speex header*/
					if (packet_count==0) {
						qa->st = process_header(&op, enh_enabled, &qa->frame_size, &qa->rate, &nframes, forceMode, &qa->channels, &stereo);
						if (!nframes) {
							nframes=1;
						}
						if (!qa->st) {
							return;
						}
					} else if (packet_count==1){
						// TODO: Debug information. To Delete?
						//print_comments((char*)op.packet, op.bytes);
						//fprintf (stderr, "File comments: ");
						//fwrite(op.packet, 1, op.bytes, stderr);
						//fprintf (stderr, "\n");
					} else {
						/* Decode the Speex packet */
						audio_size += decodePacket(qa, &op, &stereo, nframes, &eos);
						/* Do something with the decoded sound (by example, play it) */
						if (callback(qa) != 0) {
						    fclose(qa->spxfile);
						    qa->spxfile=NULL;
						    return -1;
						}
					}
					packet_count++;
				}
			}
			if (feof(qa->spxfile)) {
				break;
			}
		}
		fclose(qa->spxfile);
		qa->spxfile=NULL;
		return 0;
	}
	return 0;
}

/**
 * Internal function: Decodes one Speex data packet and saves in qa->buffer.
 *
 * \param qa quran_audio handle
 * \param *op the packet that must be decoded
 * \param *stereo speex stereo state
 * \param nframes count of frames in the packet
 * \param *eos variables where end of stream condition will be saved
 *
 * \return the audio size of this packet
 **/
int decodePacket(quran_audio *qa, ogg_packet *op, SpeexStereoState *stereo, int nframes, int *eos) {

	float output[MAX_FRAME_SIZE];
	float loss_percent=-1;
	int print_bitrate=0; // TODO: This variable never changes!!!
	int audio_size=0;
	int lost=0;
	int i,j;

	if (loss_percent>0 && 100*((float)rand())/RAND_MAX<loss_percent) {
		lost=1;
	}

	/*End of stream condition*/
	if (op->e_o_s) {
		*eos = 1;
	}

	/*Copy Ogg packet to Speex bitstream*/
	speex_bits_read_from(&qa->bits, (char*)op->packet, op->bytes);
	for (j=0;j<nframes;j++) {
		
		/*Decode frame*/
		if (!lost) {
			speex_decode(qa->st, &qa->bits, output);
		} else {
			speex_decode(qa->st, NULL, output);
		}

		if (qa->channels==2) {
			speex_decode_stereo(output, qa->frame_size, stereo);
		}

		if (print_bitrate) {
			int tmp;
			char ch=13;
			speex_decoder_ctl(qa->st, SPEEX_GET_BITRATE, &qa->bitrate);
			fputc (ch, stderr);
			fprintf (stderr, "Bitrate is use: %d bps     ", qa->bitrate);
		}

		/*PCM saturation (just in case)*/
		for (i=0;i<qa->frame_size*qa->channels;i++) {
			if (output[i]>32000.0) {
				output[i]=32000.0;
			} else if (output[i]<-32000.0) {
				output[i]=-32000.0;
			}
		}

		/*Convert to short and save to output file*/
		for (i=0;i<qa->frame_size*qa->channels;i++) {
			qa->buffer[i]=(short)le_short((short)output[i]);
		}

		// TODO: Debug information. To Delete?
		//fwrite(qa->buffer, sizeof(short), qa->frame_size*qa->channels, fout);

		audio_size += sizeof(short)*qa->frame_size*qa->channels;

	}

	return audio_size;
}

/**
 * Internal function: Processes the header of a Speex file, sets properties via
 * pointer variables and returns a decoder state
 *
 * \param ogg_packet *op the packet that contains the header
 * \param enh_enabled enhancement on/off 
 * \param *frame_size variable where the frame size will be saved
 * \param *rate variable where the rate will be saved
 * \param *nframes variable where the amount of frame will be saved
 * \param forceMode force option
 * \param *channels variable where the amount of channels will be saved
 * \param *stereo variable where speex stereo size will be saved
 *
 * \return NULL when failed recognizing the header, otherwise decoder state
 **/
static void *process_header(ogg_packet *op, int enh_enabled, int *frame_size, int *rate, int *nframes, int forceMode, int *channels, SpeexStereoState *stereo) {
	void *st;
	SpeexMode *mode;
	SpeexHeader *header;
	int modeID;
	SpeexCallback callback;

	header = speex_packet_to_header((char*)op->packet, op->bytes);
	
	if (!header) {
		fprintf (stderr, "Cannot read header\n");
		return NULL;
	}
	
	if (header->mode >= SPEEX_NB_MODES) {
		fprintf (stderr, "Mode number %d does not (any longer) exist in this version\n", header->mode);
		return NULL;
	}

	modeID = header->mode;
	if (forceMode!=-1) {
		modeID = forceMode;
	}

	mode = speex_mode_list[modeID];

	if (mode->bitstream_version < header->mode_bitstream_version) {
		fprintf (stderr, "The file was encoded with a newer version of Speex. You need to upgrade in order to play it.\n");
		return NULL;
	}

	if (mode->bitstream_version > header->mode_bitstream_version) {
		fprintf (stderr, "The file was encoded with an older version of Speex. You would need to downgrade the version in order to play it.\n");
		return NULL;
	}

	st = speex_decoder_init(mode);
	speex_decoder_ctl(st, SPEEX_SET_ENH, &enh_enabled);
	speex_decoder_ctl(st, SPEEX_GET_FRAME_SIZE, frame_size);

	callback.callback_id = SPEEX_INBAND_STEREO;
	callback.func = speex_std_stereo_request_handler;
	callback.data = stereo;
	speex_decoder_ctl(st, SPEEX_SET_HANDLER, &callback);

	if (!*rate) {
		*rate = header->rate;
	}

	/* Adjust rate if --force-* options are used */
	if (forceMode!=-1) {
		
		if (header->mode < forceMode) {
			*rate <<= (forceMode - header->mode);
		}
		
		if (header->mode > forceMode) {
			*rate >>= (header->mode - forceMode);
		}
	}

	speex_decoder_ctl(st, SPEEX_SET_SAMPLING_RATE, rate);

	*nframes = header->frames_per_packet;

	if (*channels==-1) {
		*channels = header->nb_channels;
	}

  // TODO: Debug information. To Delete?
	
	//fprintf (stderr, "Decoding %d Hz audio using %s mode",
	//		*rate, mode->modeName);

	if (header->vbr) {
		//fprintf (stderr, " (VBR)\n");
	} else {
		//fprintf(stderr, "\n");
		// fprintf (stderr, "Decoding %d Hz audio at %d bps using %s mode\n",
		//	*rate, mode->bitrate, mode->modeName);
	}

	free(header);

	return st;
}
