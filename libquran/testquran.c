#include <stdio.h>

#include <quran.h>


int main() {

	char *a;
	int c;
	quran * q;
	libquran_ctx ctx;

	ctx.initialized = 0;
	c = quran_init(&ctx);
	q = quran_open_index(0,0, &ctx);
	if (q == NULL) {
		fprintf(stderr,"Error initializing libquran, code %d\n", ctx.error_code);
		return 1;
	}

	fprintf(stdout, "\n#################################\n");
	fprintf(stdout, "Successfully initialized libquran\n");
	fprintf(stdout, "#################################\n\n");
	fprintf(stdout,"Qur'an Data Directory: %s\n",ctx.libquran.data_directory);
	
	a = quran_read_verse(q,1,1, &ctx);	
	fprintf(stdout,"Sura 1 Verse 1: %s\n",a);
	free(a);
	quran_close(q, &ctx);
	quran_free();
	return 0;
}


