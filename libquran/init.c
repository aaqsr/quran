/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */
#include <stdio.h>
#include <expat.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifndef _WIN32
#include <pwd.h>
#endif
#define _GNU_SOURCE
#include <string.h>

#include "quran.h"
#include "globals.h"
#include "search.h"

#include "init.h"


/********************************* Published functions **********************************/


/**
 * Initializes the quran library
 *
 * NOTE: must be called before any quran_* commands
 *
 * \param libquran_ctx libquran context 
 * \return LIBQURAN_SUCCEEDED when succeeded, otherwise error number
 *
 * \sa quran_free(), LIBQURAN_SUCCEEDED, LIBQURAN_XML_FAILED, LIBQURAN_NO_DATADIR, LIBQURAN_NO_LANGS, LIBQURAN_NO_CONF, LIBQURAN_NOT_INITIALIZED, LIBQURAN_ALREADY_INITIALIZED
 **/

int quran_init(libquran_ctx * ctx) {

	DIR *dir;
	int return_value;

	/* Can't initialize 2 times ;-) */
	if (ctx->initialized) {
		return LIBQURAN_ALREADY_INITIALIZED;
	}

	/* Reset context */
	ctx->error_code = 0;
	ctx->parent_tag = 0;
	ctx->text = NULL;
	ctx->config_step = 0;
	ctx->initialized = 0;
	ctx->results = NULL;
	ctx->libquran.data_directory = NULL;
	ctx->libquran.avail_langs = NULL;
	ctx->libquran.num_langs = 0;
	ctx->lang = -1;

	/* Load configuration file */
	return_value = load_conf_file(ctx);
	if (return_value!=LIBQURAN_SUCCEEDED) {
		return return_value;
	}

	/* Set directories from default values, if not set */
	if (ctx->libquran.data_directory == NULL) {
		ctx->libquran.data_directory = strdup(LIBQURAN_DEFAULT_DATADIR);
	}
	if ((dir = opendir(ctx->libquran.data_directory))==NULL) {
		return LIBQURAN_NO_DATADIR;
	}

	closedir (dir);
	/* Generate a list of available languages */
	return_value = generate_languages_list(ctx);
	if (return_value!=LIBQURAN_SUCCEEDED) {
		return return_value;
	}
	ctx->initialized = 1;
	return 0;
}

/**
 * Frees all memory used by libquran
 *
 * \param libquran_ctx libquran context 
 * \return LIBQURAN_SUCCEEDED when succeeded, otherwise error number
 *
 * \sa quran_init()
 **/
void quran_free(libquran_ctx *ctx) {

	int i,j;

	if (ctx->initialized==1) {

	    /* Free language packages strings */
	    for (i=0; i < ctx->libquran.num_langs;i++) {
			/* Free audio strings */
			for (j=0; j < ctx->libquran.avail_langs[i].num_audio_packages;j++) {
				free(ctx->libquran.avail_langs[i].avail_audio_packages[i].filename);
				free(ctx->libquran.avail_langs[i].avail_audio_packages[i].author);
				free(ctx->libquran.avail_langs[i].avail_audio_packages[i].copyright);
			}
			
			/* Free text stings */
			for (j=0; j < ctx->libquran.avail_langs[i].num_texts;j++) {
				free(ctx->libquran.avail_langs[i].avail_texts[i].filename);
				free(ctx->libquran.avail_langs[i].avail_texts[i].author);
				free(ctx->libquran.avail_langs[i].avail_texts[i].copyright);
			}

			/* Free tafser stings */
			for (j=0; j < ctx->libquran.avail_langs[i].num_tafsir;j++) {
				free(ctx->libquran.avail_langs[i].avail_tafsir[i].filename);
				free(ctx->libquran.avail_langs[i].avail_tafsir[i].author);
				free(ctx->libquran.avail_langs[i].avail_tafsir[i].copyright);
			}

			free(ctx->libquran.avail_langs[i].avail_audio_packages);
			free(ctx->libquran.avail_langs[i].avail_texts);
			free(ctx->libquran.avail_langs[i].avail_tafsir);
			free(ctx->libquran.avail_langs[i].code);
			free(ctx->libquran.avail_langs[i].name);
	    }

	    /* Free other properties */
	    free(ctx->libquran.data_directory);
	    free(ctx->libquran.avail_langs);

		/* clear context */
		ctx->initialized = 0;
	}
}


/********************************** Internal functions **********************************/


/**
 * Internal function: Loads the XML configurations file to libquran struct
 *
 * \param libquran_ctx libquran context 
 * \return LIBQURAN_SUCCEEDED when succeeded, otherwise error number
 **/
int load_conf_file(libquran_ctx *ctx) {

	FILE *file_handle = NULL;
	const char *whitespace = "\r\n\t\0\v", *trimmed;
	int n = 0, len, start;
	char * temp;
	struct passwd * current_user;

	/* Open LibQuran configuration file */
#ifndef _WIN32
	file_handle = fopen("./.quranrc","r");
	if (file_handle == NULL) {
	    current_user = getpwuid(getuid());
	    temp = (char *)malloc(strlen(current_user->pw_dir) + 10 /* /.quranrc\0 */ );
	    sprintf(temp, "%s/%s", current_user->pw_dir, "/.quranrc");
	    file_handle = fopen(temp, "r");
	    free(temp);
	    temp = NULL;
	}

	if (file_handle==NULL) {
		 file_handle = fopen("/etc/quran","r");
	}
#else
#ifdef _MINGW
	file_handle = fopen("C:\\WINDOWS\\quran.rc","r");
	if (file_handle == NULL) {
		file_handle = fopen("C:\\WINNT\\quran.rc","r");
	}
#endif // _MINGW
#endif // _!WIN32
	if (file_handle==NULL) {
		/* LibQuran configuration file not found */
		return LIBQURAN_NO_CONF;
	}
	getline(&temp, &n, file_handle);
	fclose(file_handle);
	file_handle = NULL;
	if (temp) {
		len = strlen (temp);
		start = 0;
		trimmed = temp;
	 	/* left trim whitespace */
		for (n = 0; n < len ; n --) {
			if (index (whitespace, trimmed [n]) != NULL) {
				start ++;
			} else {
	 			break;
			}
		}
		if (start > 0) {
			len -= start;
			trimmed += start;
		}

	 	/* right trim whitespace */
		for (n = len - 1; n >= 0 ; n --) {
			if (index (whitespace, trimmed [n]) != NULL) {
	 			len --;
			} else {
	 			break;
			}
		}
		if (len > 0) {
			ctx->libquran.data_directory = (char*) strndup (trimmed, len); 
			free (temp);
		} else {
			free (temp);
	 		return LIBQURAN_NO_DATADIR;
		}
	} else {
	 	return LIBQURAN_NO_DATADIR;
	}

	return LIBQURAN_SUCCEEDED;
}

/**
 * Internal function: This callback function is used when parsing the xml data.
 * Called when the parser finds an XML start tag.
 * It handles the known tags and gets audio and data paths
 *
 * \param *user_data used by quran context 
 * \param *name tag name
 * \param **attrs attributes
 *
 * \sa callback_cdata_config(), callback_end_config()
 **/
void callback_start_config(void *user_data, const XML_Char *name, const XML_Char **attrs) {

	libquran_ctx *ctx = user_data; 

	ctx->text = NULL;
	/* Handle tag <quran> */
	if ((strncmp(name,"quran",5)==0) && ctx->config_step==0) {
		ctx->config_step++;
	}

	/* Handle tag <datadir> */
	else if ((strncmp(name,"language",8)==0) && ctx->config_step==1) {
		/* Get the the name and the code of the language */
		while (attrs&&*attrs) {
			if (strncmp(attrs[0],"code",4)==0) {
				ctx->libquran.avail_langs[ctx->lang].code = (char *)strdup(attrs[1]);
			} else if (strncmp(attrs[0],"name",4)==0) {
				ctx->libquran.avail_langs[ctx->lang].name = (char *)strdup(attrs[1]);
			}
			attrs += 2;
		}
		ctx->config_step++;
	}

	/* Handle tag <audio> */
	else if ((strncmp(name,"audio",5)==0)&&ctx->config_step==2) {
		ctx->libquran.avail_langs[ctx->lang].avail_audio_packages = (data_component *)realloc(ctx->libquran.avail_langs[ctx->lang].avail_audio_packages, sizeof(data_component) * (ctx->libquran.avail_langs[ctx->lang].num_audio_packages+1) );
		ctx->parent_tag = 1;
		ctx->config_step++;
	}

	/* Handle tag <text> */
	else if ((strncmp(name,"text",4)==0) && ctx->config_step==2) {
		ctx->libquran.avail_langs[ctx->lang].avail_texts = (data_component *)realloc(ctx->libquran.avail_langs[ctx->lang].avail_texts, sizeof(data_component) * (ctx->libquran.avail_langs[ctx->lang].num_texts+1) );
		ctx->parent_tag = 2;
		ctx->config_step++;
	}

	/* Handle tag <tafsir> */
	else if ((strncmp(name,"tafsir",6)==0) && ctx->config_step==2) {
		ctx->libquran.avail_langs[ctx->lang].avail_tafsir = (data_component *)realloc(ctx->libquran.avail_langs[ctx->lang].avail_tafsir, sizeof(data_component) * (ctx->libquran.avail_langs[ctx->lang].num_tafsir+1) );
		ctx->parent_tag = 3;
		ctx->config_step++;
	}

	/* Handle tag <filename> */
	else if ((strncmp(name,"filename",8)==0) && ctx->config_step==3) {
		ctx->config_step++;
	}

	/* Handle tag </author> */
	else if ((strncmp(name,"author",6)==0) && ctx->config_step==3) {
	    ctx->config_step++;
	}

	/* Handle tag </copyright> */
	else if ((strncmp(name,"copyright",9)==0) && ctx->config_step==3) {
	    ctx->config_step++;
	}
}

/**
 * Internal function: This callback function is used when parsing the xml data.
 * Called when the parser finds an XML CData.
 *
 * \param *user_data user data
 * \param *data data string
 * \param len length of the data string
 *
 * \sa callback_start_config(), callback_end_config()
 **/
void callback_cdata_config(void *user_data, const XML_Char *data, int len) {
	libquran_ctx *ctx = user_data; 
    int count = 0;
    int flag = 0;

    for (count=0; count<len; count++)
		if (!isspace(data[count])) {
		    flag = 1;
	    	break;
		}

    if (flag == 0)
		return;

    /* If text is set to NULL, then there is no text to append to */
    if (ctx->text == NULL) {
		ctx->text = (char *)strndup(data,len);
	}
    /* if text already has some text, append the new text to that text */
    else {
		int currentTextLen = strlen(ctx->text);
        /* Save the current text in a new temporary pointer */
        char * currentText = ctx->text;
        /* Allocate memory for the current text plus the new text */
        ctx->text = (char *)malloc(currentTextLen + len + 1);
        /* Copy the current text to the new allocated space */
        strncpy(ctx->text, currentText, currentTextLen);
        free(currentText);
        currentText = NULL;
        ctx->text[currentTextLen] = '\0';
        /* Finally, append the new text to the current text */
        strncat(ctx->text, data, len);
        strncat(ctx->text, "\0", 1);
    }
}

/**
 * Internal function: This callback function is used when parsing the xml data.
 * Called when the parser finds an XML end tag.
 *
 * \param *user_data user data
 * \param *name tag name
 *
 * \sa callback_start_config(), callback_cdata_config()
 **/
void callback_end_config(void *user_data, const XML_Char *name) {

	libquran_ctx *ctx = user_data; 
	/* Handle tag </audio> */
	if ((strncmp(name,"audio",5)==0)&&ctx->config_step==3) {
		ctx->libquran.avail_langs[ctx->lang].num_audio_packages++;
		ctx->config_step--;
	}

	/* Handle tag </text> */
	else if ((strncmp(name,"text",4)==0) && ctx->config_step==3) {
		ctx->libquran.avail_langs[ctx->lang].num_texts++;
		ctx->config_step--;
	}

	/* Handle tag </tafsir> */
	else if ((strncmp(name,"tafsir",6)==0) && ctx->config_step==3) {
		ctx->libquran.avail_langs[ctx->lang].num_tafsir++;
		ctx->config_step--;
	}

	/* Handle tag </language> */
	else if ((strncmp(name,"language",8)==0) && ctx->config_step==2) {
		ctx->config_step--;
	}

	/* Handle tag </quran> */
	else if ((strncmp(name,"quran",5)==0) && ctx->config_step==1) {
		ctx->config_step--;
	}

	/* Handle tag </author> */
	else if ((strncmp(name,"author",6)==0) && ctx->config_step==4) {
	    switch (ctx->parent_tag) {
		    case 1: /* <audio> */
			ctx->libquran.avail_langs[ctx->lang].avail_audio_packages[ctx->libquran.avail_langs[ctx->lang].num_audio_packages].author = ctx->text;
			ctx->text = NULL;
			break;
		    case 2: /* <text> */
			ctx->libquran.avail_langs[ctx->lang].avail_texts[ctx->libquran.avail_langs[ctx->lang].num_texts].author = ctx->text;
			ctx->text = NULL;
			break;
		    case 3: /* <tafsir> */
			ctx->libquran.avail_langs[ctx->lang].avail_tafsir[ctx->libquran.avail_langs[ctx->lang].num_tafsir].author = ctx->text;
			ctx->text = NULL;
			break;
	    }
	    ctx->config_step--;
	}

	/* Handle tag </copyright> */
	else if ((strncmp(name,"copyright",9)==0) && ctx->config_step==4) {
	    switch (ctx->parent_tag) {
		    case 1: /* <audio> */
			ctx->libquran.avail_langs[ctx->lang].avail_audio_packages[ctx->libquran.avail_langs[ctx->lang].num_audio_packages].copyright = ctx->text;
			ctx->text = NULL;
			break;
		    case 2: /* <text> */
			ctx->libquran.avail_langs[ctx->lang].avail_texts[ctx->libquran.avail_langs[ctx->lang].num_texts].copyright = ctx->text;
			ctx->text = NULL;
			break;
		    case 3: /* <tafsir> */
			ctx->libquran.avail_langs[ctx->lang].avail_tafsir[ctx->libquran.avail_langs[ctx->lang].num_tafsir].copyright = ctx->text;
			ctx->text = NULL;
			break;
	    }
	    ctx->config_step--;
	}

	/* Handle tag </filename> */
	else if ((strncmp(name,"filename",8)==0) && ctx->config_step==4) {
	    switch (ctx->parent_tag) {
		    case 1: /* <audio> */
			ctx->libquran.avail_langs[ctx->lang].avail_audio_packages[ctx->libquran.avail_langs[ctx->lang].num_audio_packages].filename = ctx->text;
			ctx->text = NULL;
			break;
		    case 2: /* <text> */
			ctx->libquran.avail_langs[ctx->lang].avail_texts[ctx->libquran.avail_langs[ctx->lang].num_texts].filename = ctx->text;
			ctx->text = NULL;
			break;
		    case 3: /* <tafsir> */
			ctx->libquran.avail_langs[ctx->lang].avail_tafsir[ctx->libquran.avail_langs[ctx->lang].num_tafsir].filename = ctx->text;
			ctx->text = NULL;
			break;
	    }
	    ctx->config_step--;
	}

}

/**
 * Internal function: It generates a list of the available language
 * packages
 *
 * \param libquran_ctx libquran context 
 * \return LIBQURAN_SUCCEEDED when succeeded, otherwise error number
 *
 * \sa generate_audio_list()
 **/
int generate_languages_list(libquran_ctx *ctx) {

	int i, selected = 0; 
	int m=0;
	struct dirent **lang_list = NULL;

	/* Get a list of language packages directories, and use select_lang_package() to select this directories */

	int num_langs = scandir(ctx->libquran.data_directory,&lang_list, NULL, alphasort);
	if (num_langs>0) {
		ctx->libquran.num_langs = num_langs;
		ctx->libquran.avail_langs = (language*)malloc(num_langs*sizeof(language));

		/* Get the language package data from the conf.xml file for each package */
		for (i = 0; i < num_langs; i++) {
		    FILE *file_handle;
		    XML_Parser p;
		    char * user_data = NULL;
		    char * conf_xml;
			char *temp = malloc (strlen (ctx->libquran.data_directory) + strlen (lang_list[i]->d_name) + 2);

			if (temp) {
#if defined(_WIN32) && defined (_MINGW)
				sprintf (temp, "%s\\%s\0", ctx->libquran.data_directory, lang_list[i]->d_name);
#else
				sprintf (temp, "%s/%s\0", ctx->libquran.data_directory, lang_list[i]->d_name);
#endif	
			} else {
				ctx->error_code = LIBQURAN_NO_MEM;
	 			return LIBQURAN_NO_MEM; 
			}

			if (select_lang_package (temp, ctx) == 0) {
				free (temp);
				ctx->libquran.num_langs--;
				if (ctx->libquran.num_langs < 1)
					return LIBQURAN_NO_LANGS;
				continue;
			}
			free (temp);
		
		   	conf_xml = (char *)malloc(strlen(ctx->libquran.data_directory) + 
							 2 + /* 2 '/'s */
			    			     strlen(lang_list[i]->d_name) +
						     8 + /* "conf.xml" */
						     1 /* '\0' */
			    			    );

#if defined(_WIN32) && defined(_MINGW)
		    sprintf(conf_xml,"%s\\%s\\conf.xml", ctx->libquran.data_directory, lang_list[i]->d_name);
#else
		    sprintf(conf_xml,"%s/%s/conf.xml", ctx->libquran.data_directory, lang_list[i]->d_name);
#endif
		    ctx->config_step = 0;
		    ctx->lang = selected;
		    ctx->libquran.avail_langs[selected].num_texts = 0;
		    ctx->libquran.avail_langs[selected].avail_texts = NULL;
		    ctx->libquran.avail_langs[selected].num_tafsir = 0;
		    ctx->libquran.avail_langs[selected].avail_tafsir = NULL;
		    ctx->libquran.avail_langs[selected].num_audio_packages = 0;
		    ctx->libquran.avail_langs[selected].avail_audio_packages = NULL;
		    ctx->libquran.avail_langs[selected].code = lang_list[i]->d_name;
		    ctx->libquran.avail_langs[selected].name = NULL;
			selected ++;	
			
		    /* Open the package configuration file */
		    file_handle = fopen(conf_xml, "r");
		    free(conf_xml);
		    conf_xml = NULL;
		    if (file_handle==NULL) {
				return LIBQURAN_INVALID_PACKAGE_CONF;
		    }

		    /* Initialize parser */
		    p = XML_ParserCreate(NULL);
		    XML_SetElementHandler(p, callback_start_config, callback_end_config);
		    XML_SetCharacterDataHandler(p, callback_cdata_config);
		    XML_SetUserData(p, ctx);

		    for (;;) {
				int bytes_read;

				/* Get the buffer of the parser */
				void *buff = XML_GetBuffer(p, BUFF_SIZE);
				if (buff == NULL) {
				    XML_ParserFree(p);
			    	fclose(file_handle);
				    return LIBQURAN_XML_FAILED;
				}

				/* Read data from file to the buffer of the parser */
				bytes_read = fread(buff, 1, BUFF_SIZE, file_handle);
				if (bytes_read < 0) {
				    XML_ParserFree(p);
				    fclose(file_handle);
				    return LIBQURAN_XML_FAILED;
				}

				/* Parse the data in the parser buffer */
				if (! XML_ParseBuffer(p, bytes_read, bytes_read == 0)) {
				    XML_ParserFree(p);
				    fclose(file_handle);
				    return LIBQURAN_XML_FAILED;
				}
				if (bytes_read == 0) {
				    break; /* End Of File */
				}
		    }
		    XML_ParserFree(p);
		    fclose(file_handle);
		}

		free(lang_list);
	} else {
		/* No languages found */
		/* FIXME: it's still useful to know this value,
		   there should be a better mechanism to avoid memory leaks on
		   this and other initialization errors throughout the library */
		/* free(ctx->libquran.data_directory); */
		return LIBQURAN_NO_LANGS;
	}

	return LIBQURAN_SUCCEEDED;
}

/**
 * Internal function: callback for selecting language package directory
 *
 * \param dir directory entry
 * \param ctx quran context
 *
 * \return 1 when it's a language package directory, otherwise 0
 **/
int select_lang_package(const char *dir, libquran_ctx *ctx) {

	char * temp;
	int flag = 0;
	struct stat ftype;

	temp = malloc (strlen (dir) + 10);
	memset (temp, 0, strlen (dir) + 10);
	strncpy (temp, dir, strlen (dir));
#if defined(_WIN32) && defined(_MINGW)
	strncat(temp, "\\conf.xml", 9);
#else
	strncat(temp, "/conf.xml", 9);
#endif

	if (stat(temp, &ftype) == -1) {
	    free(temp);
	    return 0;
	}
	if (!S_ISREG(ftype.st_mode)) {
	    free(temp);
	    return 0;
	}
   	free(temp);
	return 1;
}

