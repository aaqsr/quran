/* gnomequran- Holy Quran 
 * (C) 2002 Mohammad DAMT
 *
 * $Id$
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <pthread.h>

#include "ngajiquran.h"
#include "callbacks.h"
#include "interface.h"

gboolean
check_audio_status() {
	if (audio_playing == -1) {
		post_close_audio();
	} else if (audio_playing == 1) {
		disable_buttons();
	} 
	return TRUE;
}

void
on_mn_exit_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	gnomequran_exit();
}


void
on_mn_cut_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_mn_copy_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_mn_paste_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_mn_preferences_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	GtkWidget *dlg_preferences = create_dlg_preferences ();
	gtk_object_set_data(GTK_OBJECT(frm_main),"dlg_preferences",dlg_preferences);
	gtk_widget_show (dlg_preferences);
}


void
on_mn_about_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	GtkWidget *frm_about = create_frm_about ();
	gtk_widget_show (frm_about);
}


void
on_btn_ok_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{
	on_btn_apply_clicked(button, user_data);
	gtk_widget_destroy(gtk_object_get_data(GTK_OBJECT(frm_main),"dlg_preferences"));
}


void
on_btn_apply_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *opt_sound = (GtkWidget*)GTK_OPTION_MENU(user_data);
	gint index = (gint) gtk_option_menu_get_history(GTK_OPTION_MENU(opt_sound)); 

	gchar *driver = g_list_nth_data(driver_short_list, index);
	if (driver) {
		audio_init(driver);
		gconf_client_set_string(gconf,g_strdup_printf("%s/sound_driver",GNOMEQURAN_GCONF_PATH),driver,NULL);
	}
	last_audio_error = 0;
	
}

int on_audio_played(quran_audio *qa_) {
	if (!audible) return;
	if (!audio_device) {
		if (qa_) {
			audio_format.bits = 16;
			audio_format.channels = qa_->channels;
			audio_format.rate = qa_->rate;
			audio_format.byte_format = AO_FMT_LITTLE;
			audio_device = ao_open_live(audio_driver,&audio_format,NULL);
			if (audio_device) audible=1;else {
				quran_audio_stop(qa);
				audible = 0;
				last_audio_error = -1;
				return;
			}
		}
	}
	g_mutex_lock(m);
	if (audio_device&&qa&&audible) {
		if (qa->playing) {
		//FIXME 
			ao_play(audio_device,(void*)qa->buffer,2*qa->channels*qa->frame_size);
		}
	} 
	g_mutex_unlock(m);
}



void
on_btn_play_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
	if (qa==NULL) {
		if (last_audio_error == -1) {
				GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW(frm_main), 
								GTK_DIALOG_DESTROY_WITH_PARENT, 
									GTK_MESSAGE_ERROR, 
									GTK_BUTTONS_CLOSE, 
									_("Error opening sound driver. Please try another sound driver in Preferences menu")) ;
				gtk_dialog_run (GTK_DIALOG (dialog));
				gtk_widget_destroy (dialog);
		} else {
			play_audio(GPOINTER_TO_INT(user_data));
		}
	}
}

void
on_btn_cancel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_widget_destroy(gtk_object_get_data(GTK_OBJECT(frm_main),"dlg_preferences"));
}


void
on_main_keypress                   (GtkWidget *widget, GdkEventKey *event,
                                        gpointer         user_data)
{
	if (quran_opened) {
		if (event->keyval==GDK_Left) {
			btn_prev();
		}
		else 
		if (event->keyval==GDK_Right) {
			btn_next();
		}
	}
}



void
on_btn_new_quran_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *opt_lang;
	GList *lang_items = NULL;
	GtkWidget *dlg_cl = create_dlg_cl();

	gtk_object_set_data(GTK_OBJECT(frm_main),"dlg_cl",dlg_cl);
	opt_lang = gtk_object_get_data(GTK_OBJECT(dlg_cl),"opt_lang");

	lang_items = (GList *)get_language_list();
	gtk_combo_set_popdown_strings (GTK_COMBO (opt_lang), lang_items);
	g_list_free (lang_items);
	
	gtk_widget_show (dlg_cl);
}

void
on_btn_close_quran_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	close_audio();
	close_quran();
}


void btn_prev() {
	active_aya--;
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_next"), TRUE);
	if (active_aya<1) active_aya = 1;
	display_verse(active_sura,active_aya);
}


void btn_next() {
	active_aya++;
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_next"), TRUE);
	display_verse(active_sura,active_aya);
}


void
on_btn_aya_prev_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
	btn_prev();
}


void
on_btn_aya_next_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
	btn_next();
}

void
on_btn_packages_ok_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

	GtkWidget *dlg_packages = gtk_object_get_data(GTK_OBJECT(frm_main),"dlg_packages");
	GtkWidget *opt_package = gtk_object_get_data(GTK_OBJECT(dlg_packages),"opt_package");
	picked_package[quran_opened] = strdup(gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(opt_package)->entry)));
	gtk_widget_destroy(dlg_packages);
	open_quran();
}


void
on_btn_packages_cancel_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_widget_destroy(gtk_object_get_data(GTK_OBJECT(frm_main),"dlg_packages"));
	open_quran();
}


void
on_btn_cl_ok_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

	GtkWidget *dlg_cl = gtk_object_get_data(GTK_OBJECT(frm_main),"dlg_cl");
	GtkWidget *opt_lang = gtk_object_get_data(GTK_OBJECT(dlg_cl),"opt_lang");
	picked_lang = strdup(gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(opt_lang)->entry)));
	gtk_widget_destroy(dlg_cl);
	prepare_audio();
}


void
on_btn_cl_cancel_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_widget_destroy(gtk_object_get_data(GTK_OBJECT(frm_main),"dlg_cl"));
}

void
on_opt_sura_changed               (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *opt_sura = (GtkWidget*)GTK_OPTION_MENU(user_data);
	gint index = (gint) gtk_option_menu_get_history(GTK_OPTION_MENU(opt_sura)); 
	active_sura = index+1;
	display_verse(active_sura,1);
}

void
on_btn_go_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	gchar *aya_no_txt = (gchar*)gtk_entry_get_text(GTK_ENTRY(user_data));

	active_aya = atoi(aya_no_txt);
	display_verse(active_sura,active_aya);

}

void        
on_font_selected (GnomeFontPicker *fontpicker,
				 gchar *arg1,
				gpointer user_data) {
	active_font = g_strdup(gnome_font_picker_get_font_name(fontpicker));	
	if (active_font) {
		gconf_client_set_string(gconf,g_strdup_printf("%s/font",GNOMEQURAN_GCONF_PATH),active_font,NULL);
		if (quran_opened>0) display_verse(active_sura, active_aya);
	}
}

void
on_btn_read_thru_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	if (gtk_toggle_button_get_active(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_read_thru"))) {
		read_through = 1;
	} else {
		read_through = 0;
	}

	gconf_client_set_int(gconf,g_strdup_printf("%s/read_through",GNOMEQURAN_GCONF_PATH),read_through,NULL);
}
