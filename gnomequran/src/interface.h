/* gnomequran- Holy Quran 
 * (C) 2002 Mohammad DAMT
 *
 * $Id$
 */

GtkWidget* create_frm_main (void);
GtkWidget* create_frm_about (void);
GtkWidget* create_dlg_preferences (void);
GtkWidget* create_dlg_cl (void);
