/* gnomequran- Holy Quran 
 * (C) 2002 Mohammad DAMT
 *
 * $Id$
 */

#ifndef __NGAJIQURAN_H__
#define __NGAJIQURAN_H__

#include <ao/ao.h>
#include <quran.h>
#include <gconf/gconf-client.h>

GtkWidget *frm_main;

int quran_opened, active_sura, active_aya, num_driver;
char *picked_lang, **picked_package;
gchar *active_font, *active_driver;
GList *driver_short_list;
GConfClient *gconf;

void gnomeexit();
void open_quran();
void close_quran();
void display_verse(int , int );

quran **q_all;
quran_audio *qa;
int has_audio, audio_driver, audible;
GThread *t;
GMutex *m;
ao_sample_format  audio_format;
ao_device *audio_device;
_libquran libquran;
int audio_playing, last_audio_error, current_index_playing;
int read_through;

#define GNOMEQURAN_GCONF_PATH "/apps/gnomequran/settings"
#endif
