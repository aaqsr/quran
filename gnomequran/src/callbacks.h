/* gnomequran- Holy Quran 
 * (C) 2002 Mohammad DAMT
 *
 * $Id$
 */

#include <gnome.h>
#include "ngajiquran.h"

void btn_next();
void btn_prev();


gboolean check_audio_status();
int on_audio_played(quran_audio *);

void
on_mn_exit_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mn_cut_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mn_copy_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mn_paste_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mn_preferences_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mn_about_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_btn_ok_clicked                      (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_apply_clicked                   (GtkButton       *button,
                                        gpointer         user_data);


void
on_btn_play_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_cancel_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_new_quran_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_close_quran_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_aya_prev_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_aya_next_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_packages_ok_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_packages_cancel_clicked               (GtkButton       *button,
                                        gpointer         user_data);


void
on_btn_cl_ok_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_cl_cancel_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_opt_sura_changed               (GtkButton       *button,
                                        gpointer         user_data);

void
on_btn_go_clicked               (GtkButton       *button,
                                        gpointer         user_data);


void
on_main_keypress                   (GtkWidget *widget, GdkEventKey *event,
                                        gpointer         user_data);

void        
on_font_selected (GnomeFontPicker *fontpicker,
				 gchar *arg1,
				gpointer user_data) ;

void
on_btn_read_thru_clicked               (GtkButton       *button,
                                        gpointer         user_data);


