/* gnomequran- Holy Quran 
 * (C) 2002 Mohammad DAMT
 *
 * $Id$
 */

#include <gnome.h>
#include "config.h"
#include "ngajiquran.h"
#include "callbacks.h"

void close_audio();

void disable_buttons() {
	int i;
	gchar *gchar_temp;
	GtkWidget *hbox;
	for (i=0;i<quran_opened;i++) {
		if (picked_package[i]==NULL) continue;
		gchar_temp=g_strdup_printf("handlebox1_aya_%d",i);
		hbox = gtk_object_get_data(GTK_OBJECT(frm_main),gchar_temp);
		g_free(gchar_temp);
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(hbox),"btn_play"), FALSE);
	}

	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_go"), FALSE);
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_prev"), FALSE);
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_next"), FALSE);
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_close_quran"), FALSE);
}

void enable_buttons() {
	int i;
	gchar *gchar_temp;
	GtkWidget *hbox;
	for (i=0;i<quran_opened;i++) {
		if (picked_package[i]==NULL) continue;
		gchar_temp=g_strdup_printf("handlebox1_aya_%d",i);
		hbox = gtk_object_get_data(GTK_OBJECT(frm_main),gchar_temp);
		g_free(gchar_temp);
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(hbox),"btn_play"), FALSE);
	}

	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_go"), TRUE);
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_prev"), TRUE);
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_next"), TRUE);
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_close_quran"), TRUE);
}

static void t_play_audio() {
	quran_audio_play(qa, on_audio_played);
	close_audio();
	g_thread_exit(0);
}

void play_audio(int index) {
//	if (qa) return;
	open_audio(index);
	if (current_index_playing == -1) return;
	audible = 1;
	disable_buttons();

	audio_playing = 1;
	t = g_thread_create((GThreadFunc*)t_play_audio, NULL, FALSE, NULL);
}

void audio_init(const gchar *driver) {
	audio_driver = ao_driver_id(driver);
}

void close_audio() {

	if (qa) {
		quran_audio_stop(qa);
	}

	g_mutex_lock(m);
	if (has_audio) {
		if (qa!=NULL) {
			if (audio_device) ao_close(audio_device);
			audio_device = NULL;
			quran_audio_close(qa);
			qa = NULL;
			audible = 1;
		}
	}
	g_mutex_unlock(m);
	audio_playing = -1;
}

void post_close_audio() {
	int replay = 0;
	audio_playing = 0;

	if (current_index_playing == -1) return;
	if (read_through&&qa==NULL) {
		int aya = active_aya+1;
		int max_aya;
		sura_info *temp = (sura_info*)quran_sura_info(active_sura);
		max_aya = temp->max_aya;
		free(temp);
		if (aya <= max_aya) {
			display_verse(active_sura, aya);
			play_audio(current_index_playing);
			replay = 1;
		}
	}
	enable_buttons();
	display_verse(active_sura, active_aya);
	if (replay) disable_buttons();
}

void open_audio(gint quran_index) {
	current_index_playing = -1;
	if (has_audio) {
		if (qa==NULL) {
			if (picked_package[quran_index]) {
				qa=quran_audio_open(q_all[quran_index]->lang,picked_package[quran_index],active_sura,active_aya);
				audio_device = NULL;
				audio_playing = 0;
				current_index_playing = quran_index;
			}
		} 
	}
}


int check_audio(const char *lang) {
	int i,j;

	for (i=0;i<libquran.num_audio_langs;i++) {
		if (strcmp(lang,libquran.avail_audio_langs[i])==0) {
			if (libquran.num_audio_packages[i]>0) return 1;
		}
	}
	return 0;
}

GList *get_package_list(const char *lang) {
	int i,j,add;
	GList *package_items = NULL;
	for (i=0;i<libquran.num_langs;i++) {
		if (strcmp(lang,libquran.avail_langs[i])==0) {
			for (j=0;j<libquran.num_audio_packages[i];j++) {
				package_items = g_list_append(package_items,(gpointer)libquran.audio_packages[i][j]);
			}
		}
	}
	return package_items;
	
}

GList *get_language_list() {
	int i,j,add;
	GList *lang_items = NULL;
	for (i=0;i<libquran.num_langs;i++) {
		add = 1;
		for (j=0;j<quran_opened;j++) {
			if (strcmp(q_all[j]->lang,libquran.avail_langs[i])==0) {
				add = 0;
				break;
			}
		}
		if (add)
			lang_items = g_list_append(lang_items,(gpointer)libquran.avail_langs[i]);
	}
	return lang_items;
	
}

void gnomequran_exit() {
	g_mutex_free(m);
	if (active_font!=NULL) free(active_font);
	if (active_driver!=NULL) g_free(active_driver);
	if (g_list_length(driver_short_list)>0) g_free(driver_short_list);
	close_audio();
	while (quran_opened>0) close_quran();
	free(q_all);
	if (has_audio) {
		ao_shutdown();
	}
	quran_free();
	g_object_unref(gconf);
	gtk_main_quit();
	fprintf(stderr,"exiting\n");
}

void prepare_quran_widgets() {
	
	GtkWidget 	*handlebox1,
				*vbox1, *vbox2,
				*txt_aya,
				*btn_play,
				*icon, *btn_read_thru, *reciter;
	GtkWidget *opt_sura;
	GtkWidget *opt_sura_menu, *sura_item;
	int i;
	gchar *hboxname;

	if (quran_opened>5) return;
	hboxname=g_strdup_printf("handlebox1_aya_%d",quran_opened);
	handlebox1 = gtk_hbox_new (FALSE,2);
	gtk_widget_ref (handlebox1);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), hboxname, handlebox1,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (handlebox1);
	gtk_box_pack_start (GTK_BOX (gtk_object_get_data(GTK_OBJECT(frm_main),"box_aya")), handlebox1, TRUE, FALSE, 0);
	g_free(hboxname);


	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox1);
	gtk_object_set_data_full (GTK_OBJECT (handlebox1), "vbox1", vbox1,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (handlebox1), vbox1);

	opt_sura = gtk_option_menu_new ();
	gtk_widget_ref (opt_sura);
	gtk_object_set_data_full (GTK_OBJECT (handlebox1), "opt_sura", opt_sura,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (opt_sura);
	gtk_box_pack_start (GTK_BOX(vbox1), opt_sura, FALSE, FALSE, 0);
	opt_sura_menu = gtk_menu_new ();

	for (i=0;i<114;i++) {
		gchar *sura_title = g_strdup_printf("%i - %s",i+1,q_all[quran_opened]->sura_titles[i]);
		sura_item = gtk_menu_item_new_with_label(sura_title);
		g_free(sura_title);
		gtk_menu_append(GTK_MENU(opt_sura_menu),sura_item);
		gtk_widget_show(sura_item);
	}
	
	gtk_option_menu_set_menu (GTK_OPTION_MENU (opt_sura), opt_sura_menu);
	
	txt_aya = gtk_text_view_new ();
	gtk_widget_ref (txt_aya);
	gtk_object_set_data_full (GTK_OBJECT (handlebox1), "txt_aya", txt_aya,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (txt_aya);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(txt_aya),GTK_WRAP_WORD);
	if (strncmp(q_all[quran_opened]->lang,"ar",2)==0) {
		gtk_widget_set_direction(txt_aya,GTK_TEXT_DIR_RTL);
		gtk_widget_set_direction(opt_sura,GTK_TEXT_DIR_RTL);
	}
	gtk_box_pack_start (GTK_BOX (vbox1), txt_aya, FALSE, TRUE, 0);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(txt_aya), FALSE);

	if (check_audio(q_all[quran_opened]->lang)&&picked_package[quran_opened]) {

		vbox2 = gtk_hbutton_box_new ();
		gtk_hbutton_box_set_layout_default(GTK_BUTTONBOX_START);
		gtk_widget_ref (vbox2);
		gtk_object_set_data_full (GTK_OBJECT (handlebox1), "vbox2", vbox2,
		                          (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (vbox2);
		gtk_box_pack_start(GTK_BOX(vbox1), vbox2,FALSE,FALSE,0);


		icon = gtk_image_new_from_file(g_strdup_printf("%s/gnomequran_audio.png",PACKAGE_DATA_DIR));
		gtk_widget_show (icon);
		btn_play = gtk_button_new();
		gtk_widget_ref (btn_play);
		gtk_object_set_data_full (GTK_OBJECT (handlebox1), "btn_play", btn_play,
		                          (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (btn_play);
		gtk_container_add(GTK_CONTAINER(btn_play), icon);

		
		gtk_box_pack_start(GTK_BOX(vbox2), btn_play, FALSE, FALSE, 0);
		

		read_through = gconf_client_get_int(gconf,g_strdup_printf("%s/read_through",GNOMEQURAN_GCONF_PATH),NULL);
		btn_read_thru = gtk_check_button_new_with_label(_("Read Through"));
		if (read_through) 
			gtk_toggle_button_set_active(btn_read_thru, TRUE);
		else
			gtk_toggle_button_set_active(btn_read_thru, FALSE);
		
		gtk_widget_ref (btn_read_thru);
		gtk_object_set_data_full (GTK_OBJECT (frm_main), "btn_read_thru", btn_read_thru,
		                          (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (btn_read_thru);

		gtk_box_pack_start(GTK_BOX(vbox2), btn_read_thru, FALSE, FALSE, 0);

		reciter = gtk_label_new(g_strdup_printf("Reciter: %s",quran_recitor_name(q_all[quran_opened]->lang,picked_package[quran_opened])));
		gtk_widget_ref(reciter);
		gtk_object_set_data_full (GTK_OBJECT (handlebox1), "reciter", reciter,
		                          (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (reciter);
		gtk_box_pack_start(GTK_BOX(vbox2), reciter, FALSE, FALSE, 0);


		g_signal_connect(GTK_OBJECT (btn_play), "clicked", G_CALLBACK(on_btn_play_clicked), GINT_TO_POINTER(quran_opened));
		g_signal_connect(GTK_OBJECT (btn_read_thru), "clicked", G_CALLBACK(on_btn_read_thru_clicked), NULL);
		gtk_widget_set_size_request(btn_play,20, 20);
	}
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_prev"), TRUE);
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_next"), TRUE);
	gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_close_quran"), TRUE);
	g_signal_connect(GTK_OBJECT(txt_aya),"key_press_event",G_CALLBACK(on_main_keypress),NULL);
	g_signal_connect(GTK_OBJECT (opt_sura), "changed", G_CALLBACK(on_opt_sura_changed), opt_sura);
}

void destroy_quran_widgets() {
	
	gchar *hboxname=g_strdup_printf("handlebox1_aya_%d",quran_opened);
	gtk_widget_destroy(GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(frm_main),hboxname)));
}

void display_verse(int sura, int aya) {
	int i;
	GtkWidget *txt_aya, *hbox, *opt_sura, *aya_no, *btn_go, *quran_info;
	PangoFontDescription *font_desc=NULL;
	gchar *gchar_temp;
	char *ayastr;
	ayainfo *aya_descr;

	GtkTextBuffer *buffer;

	if (active_font != NULL) font_desc = pango_font_description_from_string(active_font);
	
	aya_descr = quran_verse_info(sura, aya);
	if (aya_descr) {
		quran_info = gtk_object_get_data(GTK_OBJECT(frm_main),"status_bar");
		gnome_appbar_set_status(quran_info,g_strdup_printf(_("Juz %d "), aya_descr->juz_number));
		free(aya_descr);
	}
	
	if (aya<=0) aya = 1;
	if (sura<=0) sura = 1;
	
	if (aya==1) 
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_prev"), FALSE);
	else
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_prev"), TRUE);

	sura_info *temp = (sura_info*)quran_sura_info(active_sura);
	if (aya>=temp->max_aya) {
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_next"), FALSE);
		aya=temp->max_aya;
	}
	free(temp);
	aya_no = gtk_object_get_data(GTK_OBJECT(frm_main),"aya_no");
	btn_go = gtk_object_get_data(GTK_OBJECT(frm_main),"btn_go");
	gchar_temp = g_strdup_printf("%d",aya);
	gtk_entry_set_text(GTK_ENTRY(aya_no),gchar_temp);
	gtk_widget_set_sensitive (aya_no, TRUE);
	gtk_widget_set_sensitive (btn_go, TRUE);
	g_free(gchar_temp);

	for (i=0;i<quran_opened;i++) {
		gchar_temp=g_strdup_printf("handlebox1_aya_%d",i);
		hbox = gtk_object_get_data(GTK_OBJECT(frm_main),gchar_temp);
		g_free(gchar_temp);
		txt_aya = gtk_object_get_data(GTK_OBJECT(hbox),"txt_aya");
		opt_sura = gtk_object_get_data(GTK_OBJECT(hbox),"opt_sura");
		if (font_desc!=NULL) {
			gtk_widget_modify_font (txt_aya, font_desc);
			gtk_widget_modify_font (opt_sura, font_desc);
			if (g_ascii_strcasecmp(q_all[i],"ar")==0) pango_font_description_free (font_desc);
		}
		ayastr = quran_read_verse(q_all[i],sura, active_aya,0);
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (txt_aya));  
		gtk_text_buffer_set_text (buffer, ayastr, -1);
		free(ayastr);
		gtk_option_menu_set_history(GTK_OPTION_MENU(opt_sura),sura-1);
		if (picked_package[i]!=NULL) {
			quran_audio *qx;			
			GtkWidget *btn_play = gtk_object_get_data(GTK_OBJECT(hbox),"btn_play");
			if ((qx=quran_audio_open(q_all[i]->lang, picked_package[i], sura, aya))!=NULL) {
				quran_audio_close(qx);
				gtk_widget_set_sensitive (btn_play, TRUE);
			} else {
				gtk_widget_set_sensitive (btn_play, FALSE);
			}
		}
	}
	active_aya = aya;
	active_sura = sura;
}


void prepare_audio() {
	if (check_audio(picked_lang)) {
		GList *packages = NULL;
		GtkWidget *dlg_packages = (GtkWidget*)create_dlg_packages();
		GtkWidget *opt_packages = gtk_object_get_data(GTK_OBJECT(dlg_packages),"opt_package");
		packages = get_package_list(picked_lang);
		gtk_combo_set_popdown_strings (GTK_COMBO (opt_packages), packages);
		g_list_free(packages);
		gtk_widget_show(dlg_packages);
	} else {
		open_quran();
	}
}

void open_quran() {

	q_all[quran_opened] = quran_open(picked_lang);
	free(picked_lang);

	prepare_quran_widgets();

	quran_opened++;
	display_verse(active_sura, active_aya);
}

void close_quran() {

	quran_opened--;
	if (quran_opened<0) {
		quran_opened=0;
		return;
	}
	if (quran_opened==0) {
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_prev"), FALSE);
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_aya_next"), FALSE);
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_close_quran"), FALSE);
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"aya_no"), FALSE);
		gtk_widget_set_sensitive(gtk_object_get_data(GTK_OBJECT(frm_main),"btn_go"), FALSE);
	}
	quran_close(q_all[quran_opened]);
	if (picked_package[quran_opened]) free(picked_package[quran_opened]);
	
	destroy_quran_widgets();
}
