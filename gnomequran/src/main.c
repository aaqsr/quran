/* gnomequran- Holy Quran 
 * (C) 2002 Mohammad DAMT
 *
 * $Id$
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "ngajiquran.h"
#include "interface.h"
#include "callbacks.h"

int
main (int argc, char *argv[])
{

	int i;
#ifdef ENABLE_NLS
	bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain (PACKAGE);
#endif

	has_audio = quran_opened = 0;
	active_sura = active_aya = 1;
	q_all = NULL;
	active_font = active_driver = NULL;
	driver_short_list = NULL;

	g_thread_init(NULL);
	m = g_mutex_new();

	i = quran_init();
	libquran = quran_get_libquran();
	if (i!=0) {
		fprintf(stderr,"libquran initialization failed: %i\n",i);
		return 0;
	}
	gconf = gconf_client_get_default();
	gconf_client_add_dir(gconf, GNOMEQURAN_GCONF_PATH,GCONF_CLIENT_PRELOAD_NONE,NULL);
	active_driver = gconf_client_get_string(gconf,g_strdup_printf("%s/sound_driver",GNOMEQURAN_GCONF_PATH),NULL);
	active_font = gconf_client_get_string(gconf,g_strdup_printf("%s/font",GNOMEQURAN_GCONF_PATH),NULL);
	gnome_init ("gnomequran", VERSION, argc, argv);

	if (libquran.num_all_audio_packages>0) {
		ao_initialize();
		if (active_driver) {
			audio_init(active_driver);
			if (audio_driver>=0) has_audio = 1;
		}
	}

	q_all = malloc(libquran.num_langs*sizeof(quran));
	picked_package = malloc(libquran.num_langs*sizeof(char*));
	for (i=0;i<libquran.num_langs;i++) {
		picked_package[i] = NULL;
	}
	frm_main = create_frm_main ();
	g_signal_connect(GTK_OBJECT(frm_main),"delete_event",G_CALLBACK(on_mn_exit_activate),NULL);
	gtk_widget_show (frm_main);
	last_audio_error = 0;
	current_index_playing = -1;

	gtk_timeout_add(1000, (GtkFunction)check_audio_status, NULL);
	gtk_main ();
	return 0;
}

