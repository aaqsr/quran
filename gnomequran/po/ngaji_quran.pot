# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-04 11:02+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/interface.c:69
msgid "Al Quraan"
msgstr ""

#: src/interface.c:141
msgid "Opens a Quran"
msgstr ""

#: src/interface.c:148
msgid "Closes a Quran"
msgstr ""

#: src/interface.c:157
msgid "Back to previous verse"
msgstr ""

#: src/interface.c:165
msgid "Go to next verse"
msgstr ""

#: src/interface.c:182
msgid "Go To Aya #:"
msgstr ""

#: src/interface.c:198
msgid "Go"
msgstr ""

#: src/interface.c:252
msgid "(c) 2002"
msgstr ""

#: src/interface.c:253
msgid ""
"Holy Quran for GNOME. \n"
"http://oss.mdamt.net/ngaji_quran/\n"
"uses libquran library\n"
"XML files provided by Arab Eyes Project http://www.arabeyes.org"
msgstr ""

#: src/interface.c:274 src/interface.c:282
msgid "Preferences"
msgstr ""

#: src/interface.c:368
msgid "Choose Language: "
msgstr ""
