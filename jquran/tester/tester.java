import quran.lib.*;
import java.io.*;

/**
 * <code>tester</code> class is a test class for the libquran library.
 *
 * @author <a href="mailto:ahmedre@cc.gatech.edu">ahmed el-helw</a>
 * @version 1.0
 */
public class tester {

    /**
     * the <code>main</code> method is what tests the program.
     *
     * @param argv[] a <code>String</code> value
     */
    public static void main(String argv[]){
      int i;
      if (argv.length == 0){
         System.out.println("requires an argument of directory " +
                            "containing the .quranrc file.");
         return;
      }

      QuranInfo qi = new QuranInfo();
      if (!qi.setDataFile(argv[0] + File.separator + ".quranrc")){
         System.out.println("error! " + qi.getError());
         return;
      }
      qi.init();
      String[] t;
      String[] t1;
      System.out.println("language codes and names");
      t = qi.getLanguageCodes();
      t1 = qi.getLanguageNames();
      for (int c=0; c<t.length; c++)
         System.out.println(t1[c] + " (" + t[c] + ")");
      System.out.println("\navailable texts under enlgish:");
      t = qi.getLanguageTexts("en");
      for (int c=0; c<t.length; c++)
         System.out.println("\t" + t[c]);
      
      QuranInst englishQuran = null;
      if (t == null){ System.out.println("no english translation found!"); }
      else englishQuran = qi.openText("en", t[0]);
      if (englishQuran == null)
         System.out.println("error getting english quran...");
      else {
         System.out.println("\nSuccessfully opened english translation!");
         System.out.println("doing some tests...");
         System.out.println(englishQuran.getSuraName(1) + " - " +
                            englishQuran.getNumAyahs(1));
         for (i=1; i<=englishQuran.getNumAyahs(1); i++)
            System.out.println(i + ": " + englishQuran.getAyah(1, i));

         System.out.println("");
         System.out.println("juz number of some surahs:");
         System.out.println("last ayah of surat al-nas: " + 
                            englishQuran.getLocationJuz(114, 6));
         System.out.println("first ayah of surat al-fatiha: " +
                            englishQuran.getLocationJuz(1, 1));
         System.out.println("");
         System.out.println("some sajdah information:");
         System.out.println("sura 96 has a sajdah: " + 
                            englishQuran.hasSajdah(96));
         System.out.println("sura 10 has a sajdah: " + 
                            englishQuran.hasSajdah(10));
         System.out.println("sura 96 ayah 3 is a sajdah: " +
                            englishQuran.isSajdah(96, 3));
         System.out.println("sura 96 ayah 19 is a sajdah: " +
                            englishQuran.isSajdah(96, 19));
         int tmp[] = englishQuran.getSajdahs(10);
         System.out.print("sajdahs in surah 10: ");
         if (tmp == null) System.out.println("none found..");
         else System.out.println("error!  this shouldn't print!\n");
         tmp = englishQuran.getSajdahs(27);
         System.out.print("sajdahs in surah 27: ");
         if ((tmp == null) || (tmp.length != 1))
            System.out.println("error!  this shouldn't print!\n");
         else System.out.println(tmp[0]);
      }
      
      System.out.println("\n--\n");
      QuranInst translitQuran = null;
      t = qi.getLanguageTexts("tl");
      if (t == null) System.out.println("no transliteration texts found!");
      else translitQuran = qi.openText("tl", t[0]);
      if (translitQuran == null)
         System.out.println("warning, unable to open transliteration...");
      else {
         System.out.println("successfully opened transliteration data!");
         System.out.println("running some tests...");
         System.out.println(translitQuran.getSuraName(114) + " - " +
                            translitQuran.getNumAyahs(114));
         for (i=1; i<=translitQuran.getNumAyahs(114); i++)
            System.out.println(i + ": " + translitQuran.getAyah(114, i));

         System.out.println("");
         System.out.println("juz number of some surahs:");
         System.out.println("ayah 141 of surat al-baqarah: " +
                            translitQuran.getLocationJuz(2, 141));
         System.out.println("ayah 142 of surat al-baqarah: " +
                            translitQuran.getLocationJuz(2, 142));
         System.out.println("");
         System.out.println("some sajdah information:");
         System.out.println("sura 13 has a sajdah: " + 
                            translitQuran.hasSajdah(13));
         System.out.println("surah 13 ayah 1 is a sajdah: " +
                            translitQuran.isSajdah(13, 1));
         System.out.println("surah 13 ayah 15 is a sajdah: " +
                            translitQuran.isSajdah(13, 15));
         int tmp[] = translitQuran.getSajdahs(15);
         System.out.print("sajdahs in surah 15: ");
         if (tmp == null) System.out.println("none found..");
         else System.out.println("error!  this shouldn't print!\n");
         tmp = translitQuran.getSajdahs(22);
         System.out.print("sajdahs in surah 22: ");
         if ((tmp == null) || (tmp.length != 2))
            System.out.println("error!  this shouldn't print!\n");
         else System.out.println(tmp[0] + " and " + tmp[1]);
      }

      System.out.println("going to attempt audio stuff...");
      qi.playFile("ar", "ali_hozaify", 1, 1, argv[0]);
      String sp = qi.getError();
      if (sp == null) System.out.println("audio test successful");
      else System.out.println("audio playing error: " + sp);
      System.out.println("that's all...");
      System.exit(0);
   }
}
