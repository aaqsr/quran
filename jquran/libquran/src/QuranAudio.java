package quran.lib;

import java.io.*;

public class QuranAudio {
   
   /**
    * <code>name</code> is the name of the directory holding audio
    *
    */
   private String name;

   
   /**
    * <code>author</code> is the path of the audio.
    *
    */
   private String author;

   
   /**
    * <code>audiodir</code> is the path to the audio
    *
    */
   private String audiodir;

   
   /**
    * <code>copyright</code> is the copyright holder for the audio
    *
    */
   private String copyright;

   /**
    * Creates a new <code>QuranAudio</code> instance.
    *
    */
   public QuranAudio(){
      author = "";
      audiodir = "";
      copyright = "";
   }

   
   /**
    * <code>setAuthor</code> method sets the author
    *
    * @param auth a <code>String</code> value of the author
    */
   public void setAuthor(String auth){ author = auth; }

   
   /**
    * <code>getAuthor</code> method gets the author
    *
    * @return a <code>String</code> value of the author
    */
   public String getAuthor(){ return author; }

   
   /**
    * <code>setAudioDir</code> method sets the audio directory
    *
    * @param fn a <code>String</code> value of the dir name
    * @param path a <code>String</code> value of the path
    */
   public void setAudioDir(String fn, String path){
      name = fn;
      audiodir = path;
   }

   
   /**
    * <code>getName</code> method returns directory name
    *
    * @return a <code>String</code> value of the directory name
    */
   public String getName(){ return name; }

   
   /**
    * <code>getAudioDir</code> method returns the audio directory
    *
    * @return a <code>String</code> value of the audio directory
    */
   public String getAudioDir(){ return audiodir; }

   
   /**
    * Describe <code>setCopyright</code> method sets the copyright
    *
    * @param cr a <code>String</code> value of the copyright to set
    */
   public void setCopyright(String cr){ copyright = cr; }

   
   /**
    * <code>getCopyright</code> method gets the copyright of audio
    *
    * @return a <code>String</code> value of the copyright to get
    */
   public String getCopyright(){ return copyright; }

   
   /**
    * <code>playfile</code> method plays an auido file (experimental)
    *
    * @param sura an <code>int</code> value of the sura
    * @param ayah an <code>int</code> value of the ayah
    * @param tempdir a <code>String</code> value of a place to write
    * the file for decompression
    * @return a <code>String</code> value of any errors
    */
   public String playfile(int sura, int ayah, String tempdir){
      String s = audiodir + File.separator + "s";
      if (sura > 99) s += sura + "a";
      else s = s + ((sura < 10)? "00" : "0") + sura + "a";

      if (ayah > 99) s += ayah + ".spx";
      else s = s + ((ayah < 10)? "00" : "0") + ayah + ".spx";

      System.out.println("filepath to spx file: " + s);
      JSpeexDec jsd = new JSpeexDec();
      String[] arr = new String[2];
      arr[0] = s;
      arr[1] = tempdir + File.separator + "._tmp_decomp_spx.wav";
      try {
         jsd.main(arr);
         String[] arr2 = new String[1];
         arr2[0] = arr[1];
         SimpleAudioPlayer sap = new SimpleAudioPlayer();
         sap.main(arr2);
         File f = new File(arr2[0]);
         if (!f.delete())
            return "unable to play remove audio file!";
         return null;
      }
      catch (Exception e){
         return "unable to decompress spx file!  got exception: " + e;
      }
   }
}