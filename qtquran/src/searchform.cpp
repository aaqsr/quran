/***************************************************************************
$Id$
                          searchform.cpp  -  Implementation for searchForm class
                             -------------------
    begin                : Thu Sep 19 2002
    copyright           :  (C) Copyright 2002, Arabeyes, Mohammed Yousif
    email                : mhdyousif@gmx.net

$Date$
$Author$
$Revision$
$Source$ 

 ***************************************************************************/

/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify                              *
 *   it under the terms of the GNU General Public License as published by                           *
 *   the Free Software Foundation; either version 2 of the License, or                                  *
 *   (at your option) any later version.                                                                              *
 *                                                                                                                                *
 ***************************************************************************/

#include <qvariant.h>
#include <qheader.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qmessagebox.h>
#include <qbuttongroup.h>
#include <qcombobox.h>
#include <qradiobutton.h>
#include <qstringlist.h>
#include <quran.h>

#include "searchform.h"

/*
 *  Constructs a searchForm which is a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
searchForm::searchForm(  quran * qur, libquran_ctx * ctx, QWidget* parent,  const char* name, bool, WFlags )
    : searchwindow(parent, name)  {
    if ( !name )
	setName( "searchForm" );
    qu = qur;
    this->ctx = ctx;
    for ( int part=1 ; part<=30 ; part++ )
    	cbpart->insertItem( QString::number(part) );
    for ( int hezb=1 ; hezb<=60 ; hezb++ )
    	cbhezb->insertItem( QString::number(hezb) );
    for (int i=0; i<114; i++) {
     	cbchp->insertItem( QString::fromUtf8((QString)qu->sura_titles[i]) );
     	cbfromchp->insertItem( QString::fromUtf8((QString)qu->sura_titles[i]) );
     	cbtochp->insertItem( QString::fromUtf8((QString)qu->sura_titles[i]) );
    }
    //workaround a Qt problem
    if ((QString)qu->lang != "ar")
	lesearch->setText("    ");
    //fill cbfromverse and cbtoverse with the number of verses for the first chapter
    //Al-Fateha
    refresh_cbfromverse(0);
    refresh_cbtoverse(0);
}

/*
 *  Destroys the object and frees any allocated resources
 */
searchForm::~searchForm()  {
}

void searchForm::Close()  {
  this->done(1);
}

void searchForm::searchNow()  {
  //qDebug("ListResults->clear();");
  ListResults->clear();  //remove old results
  //qDebug("OK");
  lblResults->setText("0");
  int similarity = 0;
  int search_type = 0;
  int number_results=10;
  //decide how to match words
  if (rbexactmatch->isChecked())
	similarity=EXACT_MATCH;
  else if(rbsimilarmatch->isChecked())
	similarity=80;
  else if(rbsimilarity->isChecked())
	similarity=lesimilarity->text().toInt();
  //decide which search type to use
  if (rbcompletephrase->isChecked())
	search_type=COMPLETE_PHRASE;
  else if(rballwords->isChecked())
	search_type=ALL_WORDS;
  else if(rbanyword->isChecked())
	search_type=ANY_WORD;
//QString sss= (lesearch->text()).utf8();
  //decide where to search by calling the appropriate function
  if (rbwhole->isChecked())
	number_results=quran_search_quran(qu, (lesearch->text()).utf8(), search_type,
					  similarity, ctx);
  else if (rbpart->isChecked())
	number_results=quran_search_juzz(qu, (lesearch->text()).utf8(), cbpart->currentItem()+1,
					 search_type, similarity, ctx);
  else if (rbhezb->isChecked())
	number_results=quran_search_hezb(qu, (lesearch->text()).utf8(), cbhezb->currentItem()+1,
					 search_type, similarity, ctx);
  else if (rbchp->isChecked())
	number_results=quran_search_sura(qu, (lesearch->text()).utf8(), cbchp->currentItem()+1,
					 search_type, similarity, ctx);
  else if (rbfromto->isChecked())
	number_results=quran_search_part(qu, (lesearch->text()).utf8(),
					 cbfromchp->currentItem()+1, cbfromverse->currentItem()+1,
					 cbtochp->currentItem()+1, cbtoverse->currentItem()+1,
					 search_type, similarity, ctx);

  if (number_results==LIBQURAN_ERROR_OUT_OF_RANGE) {
  	qWarning("Error calling a quran_search_* function: out of range!");
  	//return;
  }
  if (number_results==LIBQURAN_ERROR_INVALID_ARGUMENTS) {
  	qWarning("Error calling a quran_search_* function: Invalid Arguments!");
  	//return;
  }
  if (number_results==LIBQURAN_NOT_INITIALIZED) {
  	qWarning("Error calling a quran_search_* function: libquran is not initialized!");
  	//return;
  }
  lblResults->setText( QString::number(number_results) );
  results_struct **results=quran_get_search_results(ctx);
  for (int result=0; result<number_results; result++)  {
  	if ( !ListResults->lastItem() )    //If the QListView is empty start filling the first item
  		item = new QListViewItem( ListResults, 0 );
  	else
  		item = new QListViewItem( ListResults, item );
  	item->setText( 0, trUtf8( QString::number(results[result]->sura_no) ) );
  	item->setText( 1, trUtf8( qu->sura_titles[results[result]->sura_no-1] ) );
  	item->setText( 2, trUtf8( QString::number(results[result]->aya_no) ) );
  }
  quran_free_search_results(ctx);

/*
  int tempInt = 0;
  unsigned short temp = 0;
  QString strTemp = "";
  QString strTemp2 = "";
  QStringList SeparatedWords;
  QStringList SeparatedWords2;
  QStringList FoundWords;
  QStringList grepResults;
  int maxaya;
  int sura_n = 0;
  int aya_n = 0;
  int results = 0;
  QChar letter;
  char * tmpcharp;
  for (int si=1; si<=114; si++) {
      //If the searching field is one chapter, skip the others
      if ( rbSura->isChecked() &&  (si !=(cbSura->currentItem()+1) ) )
    	    continue;

      sura_inf = quran_sura_info(si);
      maxaya= sura_inf->max_aya;
      delete(sura_inf);
      sura_inf=NULL;
      for (int ai=1; ai<=maxaya ; ai++)  {
                     if (rbSimpleSearch->isChecked())  {
			  tmpcharp = quran_read_verse(qu, si, ai,qu->has_search_tag ? 1: 0);
			  strTemp = QString::fromUtf8(tmpcharp);
			  delete(tmpcharp);
		          if ( strTemp.find( leSearch->text()  ) != -1 )  {
                                 sura_n = si;
                                 aya_n = ai;
                          }
                     } else if (rbUnsortedWords->isChecked()) {

                           tmpcharp = quran_read_verse(qu, si, ai,qu->has_search_tag ? 1: 0);
			   //Split the verse into words
                           strTemp = QString::fromUtf8(tmpcharp);
			   delete(tmpcharp);
                           //strTemp = "The fox jumped over the lazy cat";
                           strTemp = strTemp.stripWhiteSpace();
                           SeparatedWords = QStringList::split(' ', strTemp);
                           strTemp2 = leSearch->text();
                           SeparatedWords2 = QStringList::split(' ', strTemp2);
                           FoundWords.clear();
                           for ( QStringList::Iterator it2 = SeparatedWords2.begin(); it2 != SeparatedWords2.end(); ++it2 ) {
                                   for ( QStringList::Iterator it = SeparatedWords.begin(); it != SeparatedWords.end(); ++it ) {
                                           strTemp = *it;
                                           strTemp2 = *it2;
					   letter = strTemp.at(0);   //get the first char of the word
					   if ( letter.isPunct() )  //if the first char of the word is not a letter, remove it
                                                   strTemp.remove(0,1);
					   letter = strTemp.at(strTemp.length()-1);   //get the last char of the word
                                           if ( letter.isPunct() )  //if the last char of the word is not a letter, remove it
						   strTemp.remove(strTemp.length()-1,1);
					   if ( rbExactWord->isChecked() )
					   	tempInt = (strTemp == strTemp2);
					   else if ( rbSimilarWord->isChecked() )
					   	tempInt = strTemp.contains(strTemp2);
					   else if ( rbLD->isChecked() )
						;//tempInt = ( LD(strTemp, strTemp2) < (leLD->text()).toInt() );
					   if (tempInt) { //The two words are identical
                                                   grepResults = FoundWords.grep(strTemp);
                                                   if (grepResults.isEmpty() )  { //The found word is not in the list yet
                                                          FoundWords += strTemp2;
                                                          grepResults.clear();
                                                   }
                                           }
                                   }
                           }
                           if ( !FoundWords.isEmpty() )  {   //found some words
                                     if ( ((SeparatedWords2.count()) == (FoundWords.count())) )  {
                                            sura_n = si;
                                            aya_n = ai;
                                     }
                           }
                     }
                     if ( (sura_n != 0)  && (aya_n != 0) )  {

                           if ( !ListResults->lastItem() )    //If the QListView is empty start filling the first item
                                 item = new QListViewItem( ListResults, 0 );
                           else
                                 item = new QListViewItem( ListResults, item );
                           item->setText( 0, trUtf8( QString::number(aya_n) ) );
                           item->setText( 1, trUtf8( qu->sura_titles[sura_n-1] ) );
                           item->setText( 2, trUtf8( QString::number(sura_n) ) );
                           results++;
                           sura_n = 0;
                           aya_n = 0;
                     }
                     //break;
      }
  }
  lblResults->setText( QString::number(results) );
*/
}

void searchForm::getSelectedItem( )  {
  *sura_id = ((ListResults->currentItem())->text(0)).toInt() - 1;
  *aya_id = ((ListResults->currentItem())->text(2)).toInt() - 1;

}
void searchForm::setAddress(int * sura, int * aya)  {
  sura_id = sura;
  aya_id = aya;
}
void searchForm::disableOthers_Field()  {
  if (rbwhole->isChecked())  {
  	cbchp->setDisabled(1);
 	cbhezb->setDisabled(1);
	cbpart->setDisabled(1);
	cbfromchp->setDisabled(1);
	cbfromverse->setDisabled(1);
	cbtochp->setDisabled(1);
	cbtoverse->setDisabled(1);
 } else if (rbchp->isChecked())  {
  	cbchp->setEnabled(1);
	cbhezb->setDisabled(1);
	cbpart->setDisabled(1);
	cbfromchp->setDisabled(1);
	cbfromverse->setDisabled(1);	
	cbtochp->setDisabled(1);
	cbtoverse->setDisabled(1);
 } else if (rbhezb->isChecked())  {
  	cbchp->setDisabled(1);
	cbhezb->setEnabled(1);
	cbpart->setDisabled(1);
	cbfromchp->setDisabled(1);
	cbfromverse->setDisabled(1);
	cbtochp->setDisabled(1);
	cbtoverse->setDisabled(1);
 } else if (rbpart->isChecked())  {
  	cbchp->setDisabled(1);
	cbhezb->setDisabled(1);
	cbpart->setEnabled(1);
	cbfromchp->setDisabled(1);
	cbfromverse->setDisabled(1);
	cbtochp->setDisabled(1);
	cbtoverse->setDisabled(1);
 } else if (rbfromto->isChecked())  {
  	cbchp->setDisabled(1);
	cbhezb->setDisabled(1);
	cbpart->setDisabled(1);
	cbfromchp->setEnabled(1);
	cbfromverse->setEnabled(1);
	cbtochp->setEnabled(1);
	cbtoverse->setEnabled(1);
 }
}
void searchForm::switch_lesimilarity()  {
  if (rbsimilarity->isChecked())
  	lesimilarity->setEnabled(1);
  else
  	lesimilarity->setDisabled(1);

}
void searchForm::refresh_cbfromverse(int c_sura)  {
  //get the  number of verses in this chapter
  sura_info * sura_inf = quran_sura_info(c_sura+1);
  //Set cbfromchp to the new selection
  if ( c_sura != cbfromchp->currentItem() ) cbfromchp->setCurrentItem(c_sura);
  //remove all items in cbfromverse from the last item to the first one
  for (int citem=cbfromverse->count()-1 ; citem >= 0 ; citem-- )
       cbfromverse->removeItem(citem);
  //Add items to cbfromverse
  for (int citem=1 ; citem <= sura_inf->max_aya ; citem++)
       cbfromverse->insertItem(QString::number(citem));
  delete(sura_inf);
  //Set cbfromverse to the first item
  cbfromverse->setCurrentItem(0);
}
void searchForm::refresh_cbtoverse(int c_sura)  {
  //get the  number of verses in this chapter
  sura_info * sura_inf = quran_sura_info(c_sura+1);
  //Set cbtochp to the new selection
  if ( c_sura != cbtochp->currentItem() ) cbtochp->setCurrentItem(c_sura);
  //remove all items in cbfromverse from the last item to the first one
  for (int citem=cbtoverse->count()-1 ; citem >= 0 ; citem-- )
       cbtoverse->removeItem(citem);
  //Add items to cbfromverse
  for (int citem=1 ; citem <= sura_inf->max_aya ; citem++)
       cbtoverse->insertItem(QString::number(citem));
  delete(sura_inf);
  //Set cbtoverse to the first item
  cbtoverse->setCurrentItem(0);
}
