/************************************************************************
 * $Id$                                                                                                                 *
 *                                                                                                                          *
 * ------------                                                                                                          *
 * Description:                                                                                                   *
 * ------------                                                                                                          *
 *  Copyright (c) 2002-2005, Arabeyes, Mohammed Yousif                       *
 *                                                                                                                          *
 *  settings.cpp                                                                                                  *
 *  For Reading/Saving program settings                                                      *
 *                                                                                                                          *
 * -----------------                                                                                                    *
 * Revision Details:    (Updated by Revision Control System)                     *
 * -----------------                                                                                                    *
 *  $Date$                                                                                                           *
 *  $Author$                                                                                                        *
 *  $Revision$                                                                                                     *
 *  $Source$                                                                                                       *
 *                                                                                                                          *
 ************************************************************************/
 
/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                                                 *
 *                                                                                                                                *
 ***************************************************************************/
 
#include <unistd.h>
#include <pwd.h>
#include <qregexp.h>
#include <qfile.h>

#include "settings.h"

Settings::Settings()
{
  struct passwd * current_user = getpwuid(getuid());
  confFile = current_user->pw_dir + QString("/.qtquranrc");
}

bool Settings::ReadSettings()
{
  bool audioPluginEntryFound = false;
  
  QFile file(confFile);
  if ( file.open( IO_ReadOnly ) ) {
    QTextStream stream( &file );
    QString line;
    
    while ( !stream.atEnd() ) {
      line = stream.readLine();
      QRegExp rx( "audioplugin(\\s*)=(\\s*)(\\w+)" );
      if (rx.search(line) == 0) {
        audioPlugin = rx.cap(3).stripWhiteSpace();
        // TODO: check if the plugin exists, maybe not here?
        if (audioPlugin.length() != 0)
          audioPluginEntryFound = true;
      }
    }
    file.close();
  }
  
  if (!audioPluginEntryFound) {
    setDefaultAudioPlugin();
    return SaveSettings();
  } else
    return true;
}

bool Settings::SaveSettings()
{
  QString audioPluginLine = "audioplugin = " + audioPlugin;
  QFile file(confFile);
  if ( file.open( IO_ReadOnly ) ) {
    bool audioPluginEntryFound = false;
    QTextStream stream( &file );
    QStringList lines;
    QString line;
    while ( !stream.atEnd() ) {
      lines += stream.readLine();
    }
    file.close();
    
    for (int i = 0; i < lines.size(); i++) {
      QRegExp rx("(\\w+)(\\s*)=");
      if (rx.search(lines[i]) == 0) {
        if (rx.cap(1) == "audioplugin") {
          audioPluginEntryFound = true;
          lines[i] = audioPluginLine;
        }
      }
    }
    
    if (!audioPluginEntryFound) {
      lines += audioPluginLine;
    }
    
    if ( file.open( IO_WriteOnly ) ) {
        QTextStream stream(&file);
        for ( QStringList::Iterator it = lines.begin(); it != lines.end(); ++it )
            stream << *it << "\n";
        file.close();
        return true;
    } else {
        return false;
    }
  } else {
    if ( file.open( IO_WriteOnly ) ) {
        QTextStream stream(&file);
        stream << audioPluginLine << '\n';
        file.close();
        return true;
    } else {
        return false;
    }
  }
}

void Settings::setDefaultAudioPlugin()
{
  audioPlugin = "oss";
}

void Settings::setDefaultSettings()
{
  setDefaultAudioPlugin();
}
