/***************************************************************************
$Id$
                          searchform.h  -  Definition for searchForm class
                             -------------------
    begin                : Thu Sep 19 2002
    copyright           :  (C) Copyright 2002, Arabeyes, Mohammed Yousif
    email                : mhdyousif@gmx.net

$Date$
$Author$
$Revision$
$Source$ 
 
 ***************************************************************************/
/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify                              *
 *   it under the terms of the GNU General Public License as published by                           *
 *   the Free Software Foundation; either version 2 of the License, or                                  *
 *   (at your option) any later version.                                                                              *
 *                                                                                                                                *
 ***************************************************************************/

#ifndef SEARCHFORM_H
#define SEARCHFORM_H

#include <qvariant.h>
#include <qdialog.h>
#include <qlistview.h>
#include <quran.h>
#include <qvariant.h>

#include "searchwindow.h"

/**
  *@author Mohammed Yousif
  */

class searchForm : public searchwindow
{
    Q_OBJECT

public:
    searchForm( quran *, libquran_ctx *, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0);
    ~searchForm();    
    
public slots:
    void Close();
    void searchNow();
    void getSelectedItem();
    void setAddress(int *, int *);
    void disableOthers_Field();
    void switch_lesimilarity();
    void refresh_cbfromverse(int);
    void refresh_cbtoverse(int);
private:
    quran * qu;
    libquran_ctx * ctx;
    int * sura_id;
    int * aya_id;
    QListViewItem * item;
};

#endif // SEARCHFORM_H
#include <qvariant.h>
//#include <qmainwindow.h>
#include <qthread.h>
#include <quran.h>
#include <qwidget.h>
#include <qvaluelist.h>
#include "mainwindow.h"
//#include "quranview.h"
//#include "qurandoc.h"
