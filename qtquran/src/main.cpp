/***************************************************************************
$Id$
                          main.cpp  -  description
                             -------------------
    begin                : Fri Jul 12 15:53:41 EEST 2002
    copyright            : (C) 2002 by Mohammed Yousif
    email                : mhdyousif@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>

#include <qapplication.h>
#include <qfont.h>
#include <qstring.h>
#include <qtextcodec.h>
#include <qtranslator.h>

#include "qtquran.h"
#include "settings.h"
#include "../config.h"

int main(int argc, char * argv[])
{
  if (argc>1 && ((QString)argv[1]=="-v" || (QString)argv[1]=="--version")) {
    std::cout << "QtQuran version:" VERSION ", Copyright (C) 2002, 2005 Mohammed Yousif." << std::endl;
    return 0;
  } else if (argc>1 && ((QString)argv[1]=="-h" || (QString)argv[1]=="--help")) {
    std::cout << "QtQuran version:" VERSION ", Copyright (C) 2002, 2005 Mohammed Yousif." << std::endl;
    std::cout << "\nUsage: " << (QString)argv[0] << " [OPTIONS]" << std::endl;
    std::cout << "  -h, --help       Shows this help screen" << std::endl;
    std::cout << "  -v, --version    Displays version information\n" << std::endl;
    return 0;
  }
  
  QApplication a(argc, argv);
  
  QTranslator tor( 0 );
  tor.load( QString("qtquran_") + ((QString)QTextCodec::locale()).left(2), QTQURAN_SHARE_DIR "/lng" );
  a.installTranslator( &tor );
  
  Settings settings;
  settings.ReadSettings();

  QuranApp * quran = new QuranApp();
  if (quran->valid == false)
    return 1;
  quran->setSettings(settings);
  a.setMainWidget(quran);
  quran->show();
  return a.exec();
}
