<!DOCTYPE TS><TS>
<context>
    <name>QuranApp</name>
    <message>
        <source>Unknown Recitor</source>
        <translation>قاريء غير معروف</translation>
    </message>
    <message>
        <source>Sajda</source>
        <translation>سجدة</translation>
    </message>
    <message>
        <source>Hezb</source>
        <translation>حزب</translation>
    </message>
    <message>
        <source>The Holy Qur&apos;an...</source>
        <translation>القرءان الكريم...</translation>
    </message>
    <message>
        <source>Chapter </source>
        <translation>سورة </translation>
    </message>
    <message>
        <source>Part</source>
        <translation>جزء</translation>
    </message>
    <message>
        <source>The Holy Qur&apos;an Program - Version: </source>
        <translation>برنامج القرءان الكريم - الإصدارة: </translation>
    </message>
    <message>
        <source>Unknown Text</source>
        <translation>نص غير معروف</translation>
    </message>
    <message>
        <source>Fatal Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <source>No language packages are available</source>
        <translation>لم يتم العثور على أي حزمة لغات</translation>
    </message>
    <message>
        <source>The Holy Qur&apos;an Program
 Version </source>
        <translation>برنامج القرءان الكريم الإصدارة</translation>
    </message>
    <message>
        <source>
(c) Copyright 2002-2005, Arabeyes, by Mohammed Yousif</source>
        <translation type="obsolete">
(c) 2002-2005، عريايز، محمد يوسف</translation>
    </message>
    <message>
        <source>
(c) Copyright 2002-2005, Arabeyes http://www.arabeyes.org
by Mohammed Yousif &lt;mhdyousif@gmx.net&gt;</source>
        <translation type="obsolete">
تحت رخصة الـGPL 2002-2005، فريق عربايز http://www.arabeyes.org
محمد يوسف mhdyousif@gmx.net</translation>
    </message>
    <message>
        <source>
(c) Copyright 2002-2005 under the GPL license, Arabeyes http://www.arabeyes.org
by Mohammed Yousif &lt;mhdyousif@gmx.net&gt;</source>
        <translation>
2002-2005 تحت رخصة الـGPL، فريق عربايز http://www.arabeyes.org
محمد يوسف mhdyousif@gmx.net</translation>
    </message>
</context>
<context>
    <name>audioThread</name>
    <message>
        <source>The &quot;%1&quot; Audio Plugin Cannot Be Found</source>
        <translation>لم يتم العثور على مكون الصوت الخاص بـ&quot;%1&quot;</translation>
    </message>
    <message>
        <source>The &quot;%1&quot; audio plugin cannot be found in &quot;%2&quot;!</source>
        <translation>لم يتم العثور على مكون الصوت الخاص بـ&quot;%1&quot; في الدليل &quot;%2&quot;!</translation>
    </message>
    <message>
        <source>The &quot;%1&quot; Audio Plugin Cannot Be Loaded</source>
        <translation>لم يتمكن البرنامج من تحميل مكون الصوت الخاص بـ&quot;%1&quot;</translation>
    </message>
    <message>
        <source>The &quot;%1&quot; audio plugin cannot be loaded from the file &quot;%2&quot;!
%3</source>
        <translation>لم يتمكن البرنامج من تحميل مكون الصوت الخاص بـ&quot;%1&quot; من الملف &quot;%2&quot;
%3</translation>
    </message>
    <message>
        <source>Error Loading symbols</source>
        <translation>فشل في تحميل الدوال</translation>
    </message>
    <message>
        <source>Cannot load symbol  &apos;%1&apos; from the &quot;%2&quot; audio plugin from
the file %3
%4</source>
        <translation>فشل في تحميل الدالة &quot;%1&quot; من مكون الصوت الخاص بـ&quot;%2&quot;  من الملف
&quot;%3&quot;
%4</translation>
    </message>
    <message>
        <source>Error opening the audio file for verse %1 from chapter %2</source>
        <translation>لم يتم العثور على ملف الصوت الخاص بالآية رقم %1 من السورة رقم %2</translation>
    </message>
    <message>
        <source>Verse Audio file not found</source>
        <translation>لم يتم العثور على ملف الصوت</translation>
    </message>
</context>
<context>
    <name>mainwindow</name>
    <message>
        <source>|&lt;&lt;</source>
        <translation>&gt;&gt;|</translation>
    </message>
    <message>
        <source>Previous Chapter</source>
        <translation>سورة سابقة</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <source>Previous Verse</source>
        <translation>آية سابقة</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>إيقاف</translation>
    </message>
    <message>
        <source>Read</source>
        <translation>تلاوة</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <source>Next Verse</source>
        <translation>آية تالية</translation>
    </message>
    <message>
        <source>&gt;&gt;|</source>
        <translation>|&lt;&lt;</translation>
    </message>
    <message>
        <source>Next Chapter</source>
        <translation>سورة تالية</translation>
    </message>
    <message>
        <source>Sajda</source>
        <translation>سجدة</translation>
    </message>
    <message>
        <source>Part</source>
        <translation>جزء</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>خروج</translation>
    </message>
    <message>
        <source>About...</source>
        <translation>عن البرنامج...</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>بحث</translation>
    </message>
    <message>
        <source>Chapter:</source>
        <translation>سورة:</translation>
    </message>
    <message>
        <source>Recitor:</source>
        <translation>بصوت:</translation>
    </message>
    <message>
        <source>Verse:</source>
        <translation>آية:</translation>
    </message>
    <message>
        <source>The Holy Qur&apos;an Program - Version:</source>
        <translation>برنامج القرآن الكريم - الإصدارة:</translation>
    </message>
    <message>
        <source>Display Mode</source>
        <translation>نظام العرض</translation>
    </message>
    <message encoding="UTF-8">
        <source>_الحزب</source>
        <translation></translation>
    </message>
    <message encoding="UTF-8">
        <source>السورة_</source>
        <translation></translation>
    </message>
    <message>
        <source>P</source>
        <translation></translation>
    </message>
    <message>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <source>Language Package:</source>
        <translation>حزمة اللغة:</translation>
    </message>
    <message>
        <source>Text:</source>
        <translation>النص:</translation>
    </message>
</context>
<context>
    <name>searchwindow</name>
    <message>
        <source>Chapter Number</source>
        <translation>رقم السورة</translation>
    </message>
    <message>
        <source>Chapter Name</source>
        <translation>اسم السورة</translation>
    </message>
    <message>
        <source>Verse Number</source>
        <translation>رقم الآية</translation>
    </message>
    <message>
        <source>Search the Qur&apos;an</source>
        <translation>البحث في نص القرآن الكريم</translation>
    </message>
    <message>
        <source>Search Type</source>
        <translation>نوع البحث</translation>
    </message>
    <message>
        <source>Complete Phrase</source>
        <translation>جملة كاملة</translation>
    </message>
    <message>
        <source>All Words</source>
        <translation>جميع الكلمات</translation>
    </message>
    <message>
        <source>Any Word</source>
        <translation>أي من الكلمات</translation>
    </message>
    <message>
        <source>Matching Words</source>
        <translation>مطابقة الكلمات</translation>
    </message>
    <message>
        <source>Match Exact Words</source>
        <translation>تطابق تام</translation>
    </message>
    <message>
        <source>Match Similar Words</source>
        <translation>تطابق شبه تام</translation>
    </message>
    <message>
        <source>Similarity</source>
        <translation>مستوى التطابق</translation>
    </message>
    <message>
        <source>Search On</source>
        <translation>مجال البحث</translation>
    </message>
    <message>
        <source>The Whole Qur&apos;an</source>
        <translation>كامل القرآن الكريم</translation>
    </message>
    <message>
        <source>Hezb</source>
        <translation>حزب</translation>
    </message>
    <message>
        <source>Chapter</source>
        <translation>سورة</translation>
    </message>
    <message>
        <source>To:</source>
        <translation>إلى:</translation>
    </message>
    <message>
        <source>Part</source>
        <translation>جزء</translation>
    </message>
    <message>
        <source>From:</source>
        <translation>من:</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>بحث</translation>
    </message>
    <message>
        <source>Results:</source>
        <translation>النتائج:</translation>
    </message>
    <message>
        <source>Keywords:</source>
        <translation>كلمات البحث:</translation>
    </message>
    <message>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <source>100</source>
        <translation>100</translation>
    </message>
</context>
</TS>
