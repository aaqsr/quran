<!DOCTYPE TS><TS>
<context>
    <name>QuranApp</name>
    <message>
        <source>Unknown Recitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sajda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chapter </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hezb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Holy Qur&apos;an...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Holy Qur&apos;an Program - Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fatal Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No language packages are available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Holy Qur&apos;an Program
 Version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>
(c) Copyright 2002-2005 under the GPL license, Arabeyes http://www.arabeyes.org
by Mohammed Yousif &lt;mhdyousif@gmx.net&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>audioThread</name>
    <message>
        <source>The &quot;%1&quot; Audio Plugin Cannot Be Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The &quot;%1&quot; audio plugin cannot be found in &quot;%2&quot;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The &quot;%1&quot; Audio Plugin Cannot Be Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The &quot;%1&quot; audio plugin cannot be loaded from the file &quot;%2&quot;!
%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error Loading symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot load symbol  &apos;%1&apos; from the &quot;%2&quot; audio plugin from
the file %3
%4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error opening the audio file for verse %1 from chapter %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verse Audio file not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>mainwindow</name>
    <message>
        <source>|&lt;&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous Chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous Verse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next Verse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&gt;&gt;|</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next Chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chapter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recitor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verse:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Holy Qur&apos;an Program - Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sajda</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <source>_الحزب</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <source>السورة_</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language Package:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>searchwindow</name>
    <message>
        <source>Chapter Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chapter Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verse Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search the Qur&apos;an</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Whole Qur&apos;an</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hezb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>From:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Complete Phrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Any Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All Words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Matching Words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Match Exact Words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Match Similar Words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Similarity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>100</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Results:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Keywords:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
